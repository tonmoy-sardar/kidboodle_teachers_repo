import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

// Interceptors Imports
import { HttpErrorInterceptor } from './interceptors/http-error.interceptor';
import { JwtCentralInterceptor } from './interceptors/jwt-central.interceptor';
import { PathLocationStrategy, LocationStrategy, HashLocationStrategy } from '@angular/common';

// Extra Modules
import { CarouselModule } from 'ngx-owl-carousel-o';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FileUploadModule } from 'ng2-file-upload';
import { AppComponent } from './app.component';
import { Stringtodatepipe } from './string-date-pipe';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { ProfileComponent } from './views/profile/profile.component';
import { AnnouncementsComponent } from './views/announcements/announcements.component';
import { TimetableComponent } from './views/timetable/timetable.component';
import { LecturesComponent } from './views/lectures/lectures.component';
import { StudentComponent } from './views/student/student.component';
import { StudentAttendanceComponent } from './views/student-attendance/student-attendance.component';
import { AttendanceDetailsComponent } from './views/student/attendancedetails/attendancedetails.component';
import { HomeworksComponent } from './views/homeworks/homeworks.component';
import { GroupDiscussionsComponent } from './views/group-discussions/group-discussions.component';
import { AnswerSheetsComponent } from './views/answer-sheets/answer-sheets.component';
import { GradeCardsComponent } from './views/grade-cards/grade-cards.component';
import { HolidayCalenderComponent } from './views/holiday-calender/holiday-calender.component';
import { HeaderComponent } from './container/Layout/header/header.component';
import { LeftMenuComponent } from './container/Layout/left-menu/left-menu.component';
import { MainlayoutComponent } from './views/mainlayout/mainlayout.component';
import { LoginComponent } from './views/login/login.component';
import { CalendarComponent } from './shared/calendar/calendar.component';
import { FileUploadComponent } from './shared/file-upload/file-upload.component';
import { AnnouncementsDetailsComponent } from './views/announcements/announcements-details/announcements-details.component';
import { CreateHomeworkComponent } from './views/homeworks/create-homework/create-homework.component';
import { AddQuestionsComponent } from './views/homeworks/add-questions/add-questions.component';
import { SubmittedDetailsComponent } from './views/homeworks/submitted-details/submitted-details.component';
import { EditprofileComponent } from './views/editprofile/editprofile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ForgetpasswordComponent } from './views/forgetpassword/forgetpassword.component';
import { ExamsComponent } from './views/exam/exams.component';
import { CreateExamComponent } from './views/exam/create-exam/create-exam.component';
import { AddExamQuestionsComponent } from './views/exam/add-questions/add-questions.component';
import { SubmittedExamDetailsComponent } from './views/exam/submitted-exam-details/submitted-exam-details.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { StudentdetailsComponent } from './views/student/studentdetails/studentdetails.component';
import { NgxPaginationModule, PaginatePipe } from 'ngx-pagination';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ExamAddQuestionComponent } from './views/exam/exam-add-question/exam-add-question.component';
import { ListgroupdiscussionsComponent } from './views/group-discussions/listgroupdiscussions/listgroupdiscussions.component';
import { LiveclassesComponent } from './views/liveclasses/liveclasses.component';
import { LiveclasshistoryComponent } from './views/liveclasses/liveclasshistory/liveclasshistory.component';
import { CreateliveclassComponent } from './views/liveclasses/createliveclass/createliveclass.component';
import { ViewdetailsComponent } from './views/grade-cards/viewdetails/viewdetails.component';
import { SubmittedReviewDetailsComponent } from './views/homeworks/submitted-review-details/submitted-review-details.component';
import { SubmittedExamReviewDetailsComponent } from './views/exam/submitted-exam-review-details/submitted-exam-review-details.component';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProfileComponent,
    AnnouncementsComponent,
    TimetableComponent,
    LecturesComponent,
    StudentComponent,
    StudentAttendanceComponent,
    AttendanceDetailsComponent,
    HomeworksComponent,
    ExamsComponent,
    Stringtodatepipe,
    GroupDiscussionsComponent,
    EditprofileComponent,
    AnswerSheetsComponent,
    GradeCardsComponent,
    HolidayCalenderComponent,
    HeaderComponent,
    LeftMenuComponent,
    MainlayoutComponent,
    LoginComponent,
    CalendarComponent,
    FileUploadComponent,
    AnnouncementsDetailsComponent,
    CreateHomeworkComponent,
    CreateExamComponent,
    AddExamQuestionsComponent,
    AddQuestionsComponent,
    SubmittedExamDetailsComponent,
    SubmittedDetailsComponent,
    ForgetpasswordComponent,StudentdetailsComponent,ExamAddQuestionComponent,ListgroupdiscussionsComponent,
    LiveclassesComponent,
    LiveclasshistoryComponent,
    CreateliveclassComponent,
    ViewdetailsComponent,
    SubmittedReviewDetailsComponent,SubmittedExamReviewDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    CarouselModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    FileUploadModule,
    NgbModule,MatSnackBarModule,
    NgxPaginationModule,
    PdfViewerModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtCentralInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtCentralInterceptor,
      multi: true,
    },
    {provide: LocationStrategy, useClass: HashLocationStrategy},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
