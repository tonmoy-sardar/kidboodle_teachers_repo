import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.scss']
})
export class ForgetpasswordComponent implements OnInit {
  otpObj = {
    isOtp: false,
    isOtpMsg: "",
    otp: "",
    isMobileOtp: false,
    isEmailOtp: false
  };
  isOtpCorrect = false;
  isPasswordChanged = false;
  noUserMatch = false;
  isEmailExist = false;

  @ViewChild('editModal') editModal: TemplateRef<any>; // Note: TemplateRef
  @ViewChild('myModal') myModal: TemplateRef<any>;
  editModalRef: NgbModalRef;
  myModalRef: NgbModalRef;

  userForm: FormGroup;
  otpVerifyForm: FormGroup;
  changePasswordForm: FormGroup;
  constructor(
    private _loginService: LoginService,
    private modalService: NgbModal,
    private _formBuilder: FormBuilder,
    private _router: Router,
  ) {
    this.userForm = _formBuilder.group({
      email: ['', Validators.email],
      mobile: ['', [Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$"), Validators.minLength(10), Validators.maxLength(10)]]
    })

    this.otpVerifyForm = _formBuilder.group({
      emailOtp: [''],
      mobileOtp: ['']
    })

    this.changePasswordForm = _formBuilder.group({
      password: ['', Validators.required],
      cpassword: ['', Validators.required]
    })
    }

  ngOnInit(): void {
  }

  newPwdBtnClick() {


    console.log(this.userForm);

    let mobile = this.userForm.get('mobile').value?.toString();

    let email = this.userForm.get('email').value?.toString();



    if (mobile === "" && email === "") {

      alert('Please Enter email id or mobile no!');

      return;

    }



    if (this.userForm.valid) {

      if (mobile !== "") {

        let body = {

          dial_code: "+91",

          phone_no: mobile,

        }

        this._loginService.getphone_number_verify(body).subscribe((response) => {

          console.log(response);

          if (response.request_status == 1) {

            this.editModalRef  = this.modalService.open(this.editModal);



            this.otpObj.isOtpMsg = response.msg;

            this.otpObj.isOtp = true;

            this.otpObj.otp = response.results.otp;

            this.otpObj.isMobileOtp = true;

            this.otpObj.isEmailOtp  = false;

            console.log(window.atob(this.otpObj.otp));

          }

          else {

            alert(response.msg);

          }

        }, err => {

          console.log(err);

          alert('Some eror occured! Try later.');

          this._router.navigate(['/forgetpassword']);

        });

        return;

      }




      if (email !== "") {

        let body ={

          email_id:email,

          auth_provider:'schooluser',

        }

        this._loginService.emailExists(body).subscribe(res =>{

           console.log(res);

           alert('Email not exists. Please register first!');

        }, err =>{

          console.log(err);

          if(err.status==409){

           

            let params = {

              email_id: email

            }

            this._loginService.getemail_verify(params).subscribe((response) => {

              console.log(response);

              if(response.request_status == 1){

                this.otpObj.isOtpMsg = response.msg;

                        this.otpObj.isOtp = true;

                        this.otpObj.otp = response.results.otp;

                        this.otpObj.isMobileOtp = false;

                        this.otpObj.isEmailOtp = true;

                        console.log(window.atob(this.otpObj.otp));

                        this.editModalRef = this.modalService.open(this.editModal);

                      }

                    });



          }

        })



      }

  }

    // console.log(this.userForm);
    // let mobile = this.userForm.get('mobile').value?.toString();
    // let email = this.userForm.get('email').value?.toString();

    // if (mobile === "" && email === "") {
    //   alert('Please Enter email id or mobile no!');
    //   return;
    // }

    // if (this.userForm.valid) {
    //   if (mobile !== "") {
    //     let body = {
    //       dial_code: "91",
    //       phone_no: mobile,
    //     }
    //     this._loginService.getphone_number_verify(body).subscribe((response) => {
    //       console.log(response);
    //       if (response.request_status == 1) {
    //         this.editModalRef  = this.modalService.open(this.editModal);

    //         this.otpObj.isOtpMsg = response.msg;
    //         this.otpObj.isOtp = true;
    //         this.otpObj.otp = response.results.otp;
    //         this.otpObj.isMobileOtp = true;
    //         this.otpObj.isEmailOtp  = false;
    //         console.log(window.atob(this.otpObj.otp));
    //       }
    //       else {
    //         alert(response.msg);
    //       }
    //     }, err => {
    //       console.log(err);
    //       alert('Some eror occured! Try later.');
    //       this._router.navigate(['/forgetpassword']);
    //     });
    //     return;
    //   }


    //   if (email !== "") {
    //     this.isEmailExists(email);
    //     if(!this.isEmailExist){
    //       alert('Email not exists. Please register first!');
    //       this.userForm.patchValue({
    //         email:""
    //       })
    //       return
    //     }
    //     let body = {
    //       email_id: email
    //     }
    //     this._loginService.getemail_verify(body).subscribe((response) => {
    //       console.log(response);
    //       if(response.request_status == 1){

    //         this.otpObj.isOtpMsg = response.msg;
    //         this.otpObj.isOtp = true;
    //         this.otpObj.otp = response.results.otp;
    //         this.otpObj.isMobileOtp = false;
    //         this.otpObj.isEmailOtp = true;
    //         console.log(window.atob(this.otpObj.otp));
    //         this.editModalRef = this.modalService.open(this.editModal);
    //       }
    //     }, err => {
    //       console.log(err);
    //       alert('Error occured. Try after some time.');
    //       this._router.navigate(['/forgetpassword']);
    //     });

    //   }
    // }
    // else {
    //   this.markFormGroupTouched(this.userForm);
    // }
  }
  
  // isEmailExists(email){
  //   let body ={
  //     email_id:email,
  //     auth_provider:'schooluser',
  //   }
  //   this._loginService.emailExists(body).subscribe(res =>{
  //     // console.log(res);
      
  //     if(res.request_status === 1){
  //       this.isEmailExist = true;
  //     }
  //     else{
  //       this.isEmailExist = false;
  //     }
  //   }, err =>{
  //     console.log(err);
  //   })
  // }

  otpSubmitBtn() {
    
    if (this.otpVerifyForm.get('mobileOtp').value == "" && this.otpVerifyForm.get('emailOtp').value == "") {
      alert("please enter the otp!");
      return;
    }
    
    let mobileOtp = this.otpVerifyForm.get('mobileOtp').value?.toString();
    let emailOtp =  this.otpVerifyForm.get('emailOtp').value?.toString();
    
    if (mobileOtp !== "") {
      
      if (mobileOtp !== String(window.atob(this.otpObj.otp))) {
        alert("please enter correct otp!");
        return;
      }
      this.editModalRef.close();
      this.myModalRef = this.modalService.open(this.myModal);
    }
    if (emailOtp !== "") {
      if (emailOtp !== String(window.atob(this.otpObj.otp.toString()))) {
        alert("please enter correct otp!");
        return;
      }
      this.editModalRef.close();
      this.myModalRef = this.modalService.open(this.myModal);
    }
  }

  passChangeBtn_Click() {
    let password = this.changePasswordForm.get('password').value?.toString();
    let cPassword = this.changePasswordForm.get('cpassword').value?.toString();
    if (password !== cPassword) {
      alert("password and confirm password must be equal!")
      return;
    }

    if(this.changePasswordForm.valid){
      let obj = {
        email: this.userForm.get('email').value.toString(),
        phone: this.userForm.get('mobile').value.toString(),
        password: password,
        cpassword: cPassword
      }
      console.log(obj);
      
      this.forgotPassword(obj);
    }
    else{
      this.markFormGroupTouched(this.changePasswordForm);
    }  
  }

  forgotPassword(obj) {
    let body = {
      email: obj.email,
      phone: obj.phone,
      new_password: obj.password,
      confirm_password: obj.cpassword,
      auth_provider: 'schooluser',
    
    }
    console.log(body);
    this._loginService.getCnfpswdData(body).subscribe((response) => {
      console.log(response);
      if (response.request_status == 1) {
        this.isPasswordChanged = true;
        alert('Record updated successfully!');
      }

    }, err => {
      console.log(err);
      alert('You are not a valid user! Register first');
      this.myModalRef.close();
      this._router.navigate(["/login"])
      
    });
  }
  loginBtnClick(){
    this.myModalRef.close();
    this._router.navigate(['/login']);
  }
  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        control.controls.forEach(c => this.markFormGroupTouched(c));
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && (form.get(field).dirty || form.get(field).touched);
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'is-invalid': form.get(field).invalid && (form.get(field).dirty || form.get(field).touched),
      'is-valid': form.get(field).valid && (form.get(field).dirty || form.get(field).touched)
    };
  }
}
