import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { ProfileService } from 'src/app/services/profile.service';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-studentdetails',
  templateUrl: './studentdetails.component.html',
  styleUrls: ['./studentdetails.component.scss']
})
export class StudentdetailsComponent implements OnInit {
  ProfileData: any;
  hobbies:any;
  student_id: string =  localStorage.getItem("ViewDetailsId");

  starValue:any;
  progressBarValue:any;
  starPerformance:any;
  photoValue:any


  constructor(private location:Location, private profileService:ProfileService  ,config: NgbRatingConfig) { 
    config.max = 5;
    config.readonly = true;
  }

  ngOnInit(): void {
    this.getProfileData();
  }

  getProfileData() {
    var params = {
       student_id:this.student_id
    }
    this.profileService.getProfileData(params).subscribe(
      res => {
        console.log(res);
        this.ProfileData = res.results;
        this.hobbies = res.results.hobbies.hobby;
        this.starValue=res.results.performance_details.star;
        this.progressBarValue=res.results.present_percentage;
        // this.starPerformance=res.results.performance_details.performance;
        console.log(res);
        // console.log(this.ProfileData)
      })
  }

  goBack(): void {
    this.location.back();
  }

}
