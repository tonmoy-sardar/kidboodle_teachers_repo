import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {
  public studentSearch: FormGroup;
  classdetails: any;
  sectiondetails:any;
  studentnamelist: any;
  studentlist: any = [];
  searchData: any;
  page_size = 15;
  page = 1;
  numberOfPages = 6;
  constructor(private fb: FormBuilder,private studentService: StudentService,private router:Router) { }

  ngOnInit(): void {
    this.studentSearch = this.fb.group({
      classID: [''],
      sectionID: [''],
      studentName: ['']
    });
     this.studentService.teacher_student_class_list().subscribe((res)=> {
      console.log(JSON.stringify(res));
      this.classdetails = res.result;
     })

    this. getData();
  }
  get formControls() {
    return this.studentSearch.controls;
  }


  getData(){
    //let studentname = this.studentSearch.get('studentName').value;
    let searchData = {
      page_size: this.page_size,
      page: this.page,
      session:localStorage.getItem('current_session')
    }
    this.studentService.teacher_student_details_get(searchData).subscribe((res)=> {
      console.log(res);
       this.studentlist = res.results;
    })

  }

  changeClass(e: any) {
    this.studentSearch.get('studentName').setValue("");
    this.studentSearch.get('sectionID').setValue("");
    let classid = this.studentSearch.get('classID').value;
    this.studentService.teacher_class_section_list(classid).subscribe((res)=> {
      console.log(JSON.stringify(res));
      this.sectiondetails = res.result;
     })
  }

  changeSection() {
    this.studentSearch.get('studentName').setValue("");
    // let classid = this.studentSearch.get('classID').value;
    // let section = this.studentSearch.get('sectionID').value;
    // let searchData = {
    //   page_size: 0,
    //   page: 0,
    //   session: '',
    //   class: classid,
    //   section: section,
    //   id: '',
    // }
    // this.studentService.teacher_student_details_get(searchData).subscribe((res)=> {
    //   console.log('Student List' + JSON.stringify(res));
    //   // this.studentnamelist = res.results
    //   this.studentlist = res.results
    //  })
  }

  Search() {
    let classid = this.studentSearch.get('classID').value;
    let section = this.studentSearch.get('sectionID').value;
    //let studentname = this.studentSearch.get('studentName').value;
    let searchData = {
      page_size: this.page_size,
      page: this.page,
      session: '',
      class: classid,
      section: section,
      id: '',
    }
    this.studentService.teacher_student_details_get(searchData).subscribe((res)=> {
      console.log(res);
      this.studentnamelist = res.results;
       this.studentlist = res.results;
      this.numberOfPages = Math.ceil(res.count/this.page_size);
     })
  }
  changePage(moveToPage): void {
    console.log(moveToPage)
    if(moveToPage === 'NEXT'){
      this.page++;
    }else if(moveToPage === 'PREVIOUS'){
      if(this.page !== 1){
        this.page--;
      }
    }else{
      this.page = moveToPage;
    }
    this.Search();
  }

  details_click( id:any) {
    this.router.navigate(['/studentdetails']);
    localStorage.setItem("ViewDetailsId",id);
  }

}
