import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-student-attendance',
  templateUrl: './attendancedetails.component.html',
  styleUrls: ['./attendancedetails.component.scss']
})
export class AttendanceDetailsComponent implements OnInit {
  atten_review:string;
  atten_sremarks:string;
  @ViewChild('studentModal') studentModal : TemplateRef<any>;
  @ViewChild('teacherModal') teacherModal : TemplateRef<any>;

  pageSize: string = "0";
  pageNo: string = "1";
  detailsAttendanceData:any;
  constructor(private router:Router, private shared: SharedService,private modalService: NgbModal, 
    ) { }
 

   ngOnInit(): void {
    this.getData();
  }

  goBack(): void {
    this.router.navigate(['/student-attendance']);
  }

  getData(){
    let params = {
      student_id:localStorage.getItem('StudentId'),
      page_size:'0',
      start_date:localStorage.getItem('strDate'),
      end_date:localStorage.getItem('enDate')
    }
    this.shared.attendence_teacher_student_list_get(params).subscribe({
      next: (data) => {
      console.log(data);
      this.detailsAttendanceData=data.results;
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  ViewStudentRemarks(sremarks){
    if(sremarks == null){
      this.atten_sremarks = 'No Remarks From Student';
    } else {
      this.atten_sremarks = sremarks;
    }
    
    this.modalService.open(this.studentModal,{size:'lg'});
  }


  ViewTeacherRemarks(tremarks){
    if(tremarks == null){
      this.atten_review = 'No Remarks From Teacher';
    } else {
    this.atten_review =tremarks; 
    }
    this.modalService.open(this.teacherModal,{size:'lg'});
 
  }

 
}
