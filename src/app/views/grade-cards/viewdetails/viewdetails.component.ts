import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { GradecardService } from 'src/app/services/gradecard.service';


@Component({
  selector: 'app-viewdetails',
  templateUrl: './viewdetails.component.html',
  styleUrls: ['./viewdetails.component.scss']
})
export class ViewdetailsComponent implements OnInit {

  pageSize: string = '0';


  studentList:any;

  constructor(private location:Location, private router:Router, private GradecardService:GradecardService) { }

  ngOnInit(): void {

    this.getDetails();

  }

  getDetails() {
    var params = {
      page_size: this.pageSize,
      student_id:localStorage.getItem('ViewDetailsId'),
      exam:localStorage.getItem('ExamDetailsId')

    }
    this.GradecardService.getTeacherData(params).subscribe((responce) => {
      console.log(responce);
      this.studentList = responce.results;
    })
  }

  GradeDetails(){
    this.router.navigate(['/grade-cards']);
  }

  onPrint(divName) {
    const printContents = document.getElementById(divName).innerHTML;
    const originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}


  goBack(): void {
    this.location.back();
  }


}
