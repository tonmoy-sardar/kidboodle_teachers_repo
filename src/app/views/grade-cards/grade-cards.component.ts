import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { th } from 'date-fns/locale';
import { GradecardService } from 'src/app/services/gradecard.service';
import { SharedService } from 'src/app/shared.service';


@Component({
  selector: 'app-grade-cards',
  templateUrl: './grade-cards.component.html',
  styleUrls: ['./grade-cards.component.scss']
})
export class GradeCardsComponent implements OnInit {
  studentList: any
  pageSize: string = '0';
  class: any;
  currentSession: any;
  allClass = new Array();
  allSection = new Array();
  allStudent = new Array();
  class_id: string = '';
  section_id: string = '';



  @ViewChild('editModal') editModal: TemplateRef<any>;
  updateForm: any;
  session: any;
  selectedTab: string;
  viewForm: any;
  scl_class: any;
  cls_section: any;
  teachers_comment: any;
  exam: any;
  cls_section_id: any;
  allExam: any;


  constructor(private GradecardService: GradecardService, private router: Router, private modalService: NgbModal, private shared: SharedService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getStudent()
    this.getDetails();
    this.getAllClass();
    this.getExamList();
  }

  getDetails() {
    var params = {
      page_size: this.pageSize,
    }
    this.GradecardService.getTeacherData(params).subscribe((responce) => {
      console.log(responce);
      this.studentList = responce.results;
    })
  }

  getAllClass() {
    // this.updateForm.controls['cls_section'].setValue('SELECT');
    // this.viewForm.controls['cls_section'].setValue('SELECT');
    // this.viewForm.controls['subject'].setValue('SELECT');

    let params = {
      page_size: '100',
      page: '1',
      session: this.session,
    }
    this.GradecardService.getClassDetailsdata(params).subscribe({
      next: (data) => {
        console.log(data);
        if (data.request_status == 1) {
          this.allClass = data['result'];
        }
      },
      error: (error) => {
        this.snackBar.open(error, 'close', {
          duration: 5000,
        });
      },
      complete: () => {
      }
    });
  }

  getAllSections() {
    let params = {
      page_size: '100',
      page: '1',
      session: localStorage.getItem('session_id'),
      class: this.class_id
    }
    this.GradecardService.getSectiondata(params).subscribe((responce) => {
      console.log(responce);
      this.allSection = responce.result;
    });
  }

  getExamList() {
    var params = {
      page_size: this.pageSize
    }
    this.GradecardService.getExamdata(params).subscribe((responce) => {
      console.log(responce);
      this.allExam = responce.results;
    });
  }

  getStudent() {
    const params = {
      page_size: 0
    }
    this.GradecardService.getStudentDetailsdata(params).subscribe(responce => {
      console.log(responce);
      this.allStudent = responce.results;

    })
  }

  classChange(e) {

    let params = {
      page_size: '100',
      page: '1',
      session: localStorage.getItem('session_id'),
      class: this.class_id,

    }
    this.GradecardService.getSectiondata(params).subscribe((responce) => {
      console.log(responce);
      this.allSection = responce.result;
    });

    this.getStudent();
    this.sectionChange(e);
  }

  changeSelectedTab(tab): void {
    this.selectedTab = tab;
  }


  sectionChange(e) {
    this.cls_section = this.updateForm.value.cls_section;


    const params = {
      scl_class: this.updateForm.value.scl_class,
      cls_section: this.updateForm.value.cls_section,
    }
    this.GradecardService.getTeacherData(params).subscribe({
      next: (data) => {
        this.allStudent = data.result;
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    console.log(params);
  }


  onSearch() {
    var params = {
      page_size: this.pageSize,
      exam: localStorage.getItem('ExamDetailsId'),
      scl_class: this.scl_class,
      cls_section: this.cls_section_id,
      student_id: localStorage.getItem('ViewDetailsId'),
    }

    this.GradecardService.getAlldata(params).subscribe((responce) => {
      this.studentList = responce.results;

    }
    )
  }





  viewDetails(student: any,exam:any) {
    localStorage.setItem('ViewDetailsId',student);
    localStorage.setItem('ExamDetailsId', exam);
    this.router.navigate(['/grade-cards/viewdetails']);
  }

  openModel(id:any) {
    this.modalService.open(this.editModal, { size: 'lg' });

  }

  goBack(): void {
    this.router.navigate(['/']);
  }

}
