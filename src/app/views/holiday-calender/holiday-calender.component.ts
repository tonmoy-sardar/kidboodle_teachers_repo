import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HolidaycalendarService } from 'src/app/services/holidaycalendar.service';

@Component({
  selector: 'app-holiday-calender',
  templateUrl: './holiday-calender.component.html',
  styleUrls: ['./holiday-calender.component.scss']
})
export class HolidayCalenderComponent implements OnInit {

  holidayList: any;
  holidayYear: any;
  holidayMonth: any;

  pageSize: string = '0';
  date: string = "";
  session: string = "";
  month: string = "";
  currentYear: number=new Date().getFullYear();
  current_session: string = '';

  public a: any;

  public months = [
    {
      id: 1,
      text: 'Jan'
    },
    {
      id: 2,
      text: 'Feb'
    },
    {
      id: 3,
      text: 'Mar'
    },
    {
      id: 4,
      text: 'Apr'
    },
    {
      id: 5,
      text: 'May'
    },
    {
      id: 6,
      text: 'Jun'
    },
    {
      id: 7,
      text: 'Jul'
    },
    {
      id: 8,
      text: 'Aug'
    },
    {
      id: 9,
      text: 'Sep'
    },
    {
      id: 10,
      text: 'Oct'
    },
    {
      id: 11,
      text: 'Nov'
    },
    {
      id: 12,
      text: 'Dec'
    },
  ];


  constructor(private holidayCalendarService: HolidaycalendarService, private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.session = localStorage.getItem('session_id');
    this.current_session = localStorage.getItem('current_session');
    console.log(this.current_session);
    this.GetHolidayList();

  }

  GetHolidayList() {
    var params = {
      page_size: this.pageSize,
      session: this.session,
      month: this.month,
      year: this.currentYear,
      date: this.date,
    }
    this.holidayCalendarService.getHolidayList(params).subscribe(
      res => {
        console.log(res);
        this.holidayList = res.results;
      })
  }

  onClick(id: any) {
    var params = {
      page_size: '100',
      session: localStorage.getItem('session_id'),
      month: id,
      year: this.currentYear,
      date: this.date,
    }
    this.holidayCalendarService.getHolidayList(params).subscribe(
      res => {
        console.log(res);
        this.holidayList = res.results;
      })
  }
  goBack(): void {
    //this.router.navigate(['/']);
    this.location.back();
  }

}
