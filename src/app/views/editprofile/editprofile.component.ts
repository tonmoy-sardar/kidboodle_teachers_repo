import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { ProfileService } from 'src/app/services/profile.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit {
   currentSession: string = localStorage.getItem("CurrentSession");
  sub: Subscription = new Subscription();
  isSuccess:boolean = false;
  profileObj = {
    address1:"",
    address2:"",
    phone:"",
    country_code: 1,
    state_code:"",
    city_code:"",
    pin_code:"",
    dial_code:"",
    city_name:""
    
  };
  allCountry = new Array();
  allStates = new Array();
  allCities = new Array();
  fieldDisabled: boolean = true;
  allClass = new Array();
  allSection = new Array();
  userForm: FormGroup;
  Countryid:any;
  Stateid:any;
  Cityid:any;
  dialCode:any;

  constructor(
    private _formBuilder: FormBuilder,
    private Service: ProfileService,
    private _router: Router,
    private sharedService:SharedService
   ) { }

  ngOnInit(): void {
   
    this.buildForm();

     this.getData();
   
  }
  buildForm() {
    this.userForm = this._formBuilder.group({
      address1: ['', Validators.required],
      address2: [''],
      phone: ['', [Validators.required,Validators.maxLength(10), Validators.minLength(10)]],
    })
  }

  getData() {     
      this.Service.teacher_profile_details().subscribe((responce) => {
        console.log(responce);
        if(responce.request_status == 1){
          
          this.profileObj.address1 = responce.results.address1;
          this.profileObj.address2 = responce.results.address2;
          this.profileObj.phone = responce.results.phone;
          this.profileObj.country_code = responce.results.country_code;
          this.profileObj.state_code = responce.results.state_code;
          this.profileObj.city_code = responce.results.city_code;
          this.profileObj.pin_code = responce.results.pin_code;
          this.profileObj.dial_code = "+91"; 
          this.getAllCountry();
          this.getAllState(this.profileObj.country_code);
        
            this.Service.getCityService(1).subscribe(data => {
              this.allCities = data['result'];
             
            }, err => {
              console.log(err);
            })  
         }
      })
  
  }

  getAllCountry() {
    this.Service.getCountryService().subscribe(data => {
      this.allCountry = data['result'];
      // console.log(this.allCountry[0].name);

    // this.Countryid = this.allCountry.find(e => e.name == this.profileObj.country_code).id;
    // this.profileObj.country_code=this.Countryid;
    //  console.log(this.profileObj.country_code);
     this.getAllState(this.profileObj.country_code);

    }, err => {
      console.log(err);
    });
  }

  // get state for a particular country
  getAllState(country_id) {
    console.log(country_id)
    this.Service.getStateService(country_id).subscribe(data => {
      this.allStates = data['result'];

    }, err => {
      console.log(err);
    })
  }
  // get city for a particular state
  getAllCity(state_id) {
    // console.log(state_id);
    this.Service.getCityService(state_id).subscribe(data => {
      this.allCities = data['result'];
     
    }, err => {
      console.log(err);
    })
  }

  countryChange(e) {
    this.userForm.patchValue({
      state_code: '',
      city_code: ''
    })
    this.getAllState(e.target.value);
  }

  stateChanged(e) {
    this.userForm.patchValue({
      city_code: ''
    })
    this.getAllCity(e.target.value);
  }

  submitStudent_click() {
    console.log(this.profileObj);
    if (this.userForm.valid) {
      const params = new HttpParams()
      .set('method','edit');

      let body ={
        address1:this.userForm.get('address1').value,
        address2:this.userForm.get('address2').value,
        phone:this.userForm.get('phone').value,    
      }
      console.log(body);
      this.Service.putProfileData(params,body).subscribe((responce) => {
        console.log(responce);
        if (responce.request_status == 1) {
          this.isSuccess = true;
          this.sharedService.openSnackBar('Record saved successfully!','X',this.isSuccess);
        } else {
          this.isSuccess = false;
          this.sharedService.openSnackBar(responce.msg,'X',this.isSuccess);
        }
      },
        error => {
          console.log(error);

        })       
    }
    else {
      alert("Please fill required fields!");
      this.isSuccess = false;
      this.sharedService.openSnackBar('Please fill required fields!','X', this.isSuccess);
    }
  }
  btnBack_click() {
    this._router.navigate(['/profile']);
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  applyCss(form:FormGroup, field:string)
  {
    return this.sharedService.displayFieldCss(form,field);
  }
  validField(form:FormGroup,field:string)
  {
    return this.sharedService.isFieldValid(form,field);
  } 
}