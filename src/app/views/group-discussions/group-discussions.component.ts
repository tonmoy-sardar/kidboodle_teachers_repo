import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupDiscussionService } from 'src/app/services/group-discussion.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-group-discussions',
  templateUrl: './group-discussions.component.html',
  styleUrls: ['./group-discussions.component.scss']
})
export class GroupDiscussionsComponent implements OnInit {
  currentSession = localStorage.getItem("session_id");
  gdForm:FormGroup;

  allClassList=[];
  allSectionList=[];
  allSubjectList=[];
  allGdLists=[];
  allUpcomingGdList=[];

  imageURL;
  disableDate;
  isSuccess:boolean = false;
  isGdediting = false;
  
  pageSize=10;
  page=1;
  totalItems;

  updateGd={
    attachment_1: "",
    attachment_2: "",
    class_name: "",
    cls_section: "",
    date: "",
    end_time: "",
    id: "",
    is_active: false,
    school_user: "",
    scl_class: "",
    section: "",
    start_time: "",
    subject: "",
    subject_name: "",
    title: "",
    zoom_link: ""
  };
  
  constructor(
    private shared:SharedService,
    private fb:FormBuilder,
    private router:Router,
    private gdService:GroupDiscussionService,
    private actRoute:ActivatedRoute
  ) {
      this.gdForm = this.fb.group({
        title:["", Validators.required],
        stime:["",Validators.required],
        etime:["",Validators.required],
        date:["",Validators.required],
        class:["", Validators.required],
        section:["",Validators.required],
        subject:["", Validators.required],
        zoomlink:["",Validators.required],
        attachment_1:[null],
        attachment_2:[null],
      })
    }

  ngOnInit(): void {
    this.disableDate = this.disableDates();
    this.listAllGroupDiss();
    this.getAllClass();
  }

  goBack(){
    this.router.navigate(['/']);
  }

  groupDiscussionsHistory(){
    this.router.navigate(['/group-discussions-history']);
  }

  disableDates(){
    var dtToday = new Date();
    var month = dtToday.getMonth() + 1;     // getMonth() is zero-based
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
      var mainmonth = '0' + month.toString();
    if(day < 10)
      var mainday = '0' + day.toString();

    var maxDate = year + '-' + mainmonth + '-' + mainday;
    return maxDate;
  }

  getAllClass() {
    let params = {
      page_size: '',
      session: this.currentSession,
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allClassList = data['result'];
          // console.log(this.allClassList);
        }
      },
      error: (error) => {
        console.error(error);
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  onClassChange(e){
    let classId = e.target.value;
    this.getAllSections(classId);
    this.getAllSubjects(classId);
  }

  getAllSections(classId) {
    let params = {
      session: this.currentSession,
      class: classId
    }
    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        // console.log(data);
        if(data.request_status == 1){
          this.allSectionList = data['result'];
          // console.log(this.allSectionList);
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
      }
    });
  }

  getAllSubjects(classId) { 
    let params = {
      session:this.currentSession,
      scl_class: classId
    }
    return this.shared.school_subject_class_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSubjectList = data['result'];
          // console.log(this.allSubjectList);
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  };

  listAllGroupDiss(){
    const params = new HttpParams()
    .set('page_size', '100');
    
    this.gdService.getAllGroupDiscussion(params).subscribe(data => {
      if(data.request_status === 1){
        this.allGdLists = data['results'];
        if(this.allGdLists.length > 0){
          this.allUpcomingGdList = this.allGdLists.filter(elem => elem.is_upcoming == true);
          this.totalItems = this.allUpcomingGdList.length;
          console.log(this.allUpcomingGdList);
        }
      }
    }, err =>{
      this.shared.openSnackBar('Internal server error!', 'X', false);
      console.log(err);
    })
  }

  showPreview1(event) {
    const file = event.target.files[0];
    // console.log(file);
    this.gdForm.patchValue({
      attachment_1: file
    });
    this.gdForm.get('attachment_1').updateValueAndValidity();
    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.updateGd.attachment_1 = reader.result as string;
    }
    reader.readAsDataURL(file);
  }
  showPreview2(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.gdForm.patchValue({
      attachment_2: file
    });
    this.gdForm.get('attachment_2').updateValueAndValidity();
    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.updateGd.attachment_2 = reader.result as string;
    }
    reader.readAsDataURL(file);
  }

  convertFormdataFunc(body){
    var formData = new FormData();
    formData.append("title",body.title);
   
    formData.append("date",body.date);
    formData.append("start_time",body.start_time);
    formData.append("end_time",body.end_time);
    formData.append("zoom_link",body.zoom_link);

    if(body.scl_class != null || body.scl_class != ""){
      formData.append("scl_class", body.scl_class);
    }
    else{
      formData.append("scl_class", "");
    }

    if(body.cls_section != null || body.cls_section != ""){
      formData.append("cls_section",body.cls_section);
    }
    else{
      formData.append("cls_section", "");
    }

    if(body.subject != null || body.subject != ""){
      formData.append("subject",body.subject);
    }
    else{
      formData.append("subject","");
    }

    if(body.attachment_1 != null){
      formData.append("attachment_1",body.attachment_1);
    } else {
      formData.append("attachment_1","");
    }

    if(body.attachment_2 != null){
      formData.append("attachment_2",body.attachment_2);
    } else {
      formData.append("attachment_2","");
    }

    return formData;
  }

  saveBtn_Click(){
  if(this.gdForm.valid){
      if(this.isGdediting == false){
        // adding a new gd
        let date=this.gdForm.get('date').value;
        let start_time=this.gdForm.get('stime').value;
        let end_time= this.gdForm.get('etime').value;
         
          let body = {
            title:this.gdForm.get('title').value,
            scl_class:this.gdForm.get('class')?.value,
            cls_section:this.gdForm.get('section')?.value,
            subject:this.gdForm.get('subject')?.value,
            date,
            start_time: date + " " + start_time,
            end_time : date + " " + end_time,
            zoom_link:this.gdForm.get('zoomlink').value,
            attachment_1:this.gdForm.get('attachment_1').value,
            attachment_2:this.gdForm.get('attachment_2').value
          }
          console.log(body);
          let formValueBody = this.convertFormdataFunc(body);
          console.log(formValueBody);
          this.gdService.creategroupDiscussion(formValueBody).subscribe(data => {
            console.log(data);
              this.shared.openSnackBar('Group disscussion added successfully!','X',true);
              this.listAllGroupDiss();
              this.gdForm.reset();
          },
          err =>{
            this.shared.openSnackBar('Internal server error!','X',false);
            console.log(err);
          })
      } 
      else {
        // updating a gd
        let body = {
          subject_name: this.updateGd?.subject_name,
          class_name: this.updateGd?.class_name,
          section: this.updateGd?.section,
          title: this.updateGd?.title,
          date: this.updateGd?.date,
          zoom_link: this.updateGd?.zoom_link,
          is_active: this.updateGd?.is_active,
          school_user: this.updateGd?.school_user,
          scl_class: this.updateGd?.scl_class,
          cls_section: this.updateGd?.cls_section,
          subject: this.updateGd?.subject,
        };
        body['start_time'] = this.updateGd.date + " " + this.updateGd.start_time; 
        body['end_time'] = this.updateGd.date + " " + this.updateGd.end_time;
        console.log(body);
        
        this.gdService.updateGroupDiscussion(this.updateGd.id,body).subscribe(data =>{
          console.log(data);
          this.shared.openSnackBar("updated successfully!","X",true);
          this.isGdediting = false;
          this.listAllGroupDiss();
        }, err =>{
          console.log(err);
        })
      }
  }
  else{
      this.shared.openSnackBar('please enter all the fields!','X',false);
      this.shared.markFormGroupTouched(this.gdForm);
      }
  }

  

  editBtnClick(gd){
 
    console.log(gd);
    this.isGdediting = true;

    let start_time = this.handleTime(gd.start_time);
    let end_time = this.handleTime(gd.end_time);

    this.updateGd={
      attachment_1: gd.attachment_1,
      attachment_2: gd.attachment_2,
      class_name: gd?.class_name,
      cls_section: gd?.cls_section,
      date: gd.date,
      end_time: end_time,
      id: gd.id,
      is_active: gd.is_active,
      school_user: gd.school_user,
      scl_class: gd?.scl_class,
      section: gd?.section,
      start_time: start_time,
      subject: gd?.subject,
      subject_name: gd?.subject_name,
      title: gd.title,
      zoom_link: gd.zoom_link
    };
    this.getAllSections(gd.scl_class);
    this.getAllSubjects(gd.scl_class);
  }
  
  deleteBtnClick(gd){
    this.gdService.deleteGroupDisscussion(gd.id).subscribe( data =>{
      console.log(data);
      this.shared.openSnackBar("Successfully removed!","X",true);
      this.listAllGroupDiss();
    }, err =>{
      console.log(err);
    })
  }
  
  handleTime = (gd) => {
    let data= new Date(gd);
    let hrs = data.getHours();
    
    let mins = data.getMinutes();
    let mainhrs;
    let mainmins;
    if(hrs<=9)
       mainhrs = '0' + hrs;
    else{
      mainhrs = hrs;
    }
    if(mins<10)
      mainmins = '0' + mins;
      else{
        mainmins = mins
      }
    const postTime= mainhrs + ':' + mainmins
    return postTime;
  }

  startCallBtn(gd){
    window.open(gd.zoom_link,"_blank","toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=600,height=600");
  }

  handlePageChange(event){
    this.page = event;
  }
  
  applyCss(form:FormGroup, field:string)
  {
    return this.shared.displayFieldCss(form,field);
  }
  validField(form:FormGroup,field:string)
  {
    return this.shared.isFieldValid(form,field);
  }
}


