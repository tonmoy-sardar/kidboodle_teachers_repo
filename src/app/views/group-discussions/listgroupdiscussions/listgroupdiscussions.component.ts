import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GroupDiscussionService } from 'src/app/services/group-discussion.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-listgroupdiscussions',
  templateUrl: './listgroupdiscussions.component.html',
  styleUrls: ['./listgroupdiscussions.component.scss']
})
export class ListgroupdiscussionsComponent implements OnInit {
  
  allGdLists=[];
  
  isSuccess:boolean = false;
  totalItems;
  page: number = 1;
  pageSize:number = 10;
  constructor(
    private router:Router,
    private gdService:GroupDiscussionService,
    private shared: SharedService
  ) { }

  ngOnInit(): void {
    this.listAllGroupDiss();
  }
  
  goBack(){
    this.router.navigate(['/group-discussions']);
  }
  
  listAllGroupDiss(){
    const params = new HttpParams()
    .set('page_size', '100');
    
    this.gdService.getAllGroupDiscussion(params).subscribe(data => {
      console.log(data);
      if(data.request_status === 1){
        this.allGdLists = data['results'].filter((elem:any) => elem.is_upcoming == false);
        //this.allGdLists = data['results'];

        this.totalItems = data.count;
      }
    }, err =>{
      this.isSuccess = false;
      this.shared.openSnackBar('Error occured!. Try after some time.', 'X', this.isSuccess);
      console.log(err);
    })
  }

  handlePageChange(event){ 
      this.page = event;
  }
}
