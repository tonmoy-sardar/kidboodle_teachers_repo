import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListgroupdiscussionsComponent } from './listgroupdiscussions.component';

describe('ListgroupdiscussionsComponent', () => {
  let component: ListgroupdiscussionsComponent;
  let fixture: ComponentFixture<ListgroupdiscussionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListgroupdiscussionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListgroupdiscussionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
