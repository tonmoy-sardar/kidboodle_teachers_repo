import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../auth/authentication.service';
import { first } from 'rxjs/operators';
import { SharedService } from 'src/app/services/shared.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isSuccess:boolean = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private sharedService:SharedService,
    
  ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  validateForm!: FormGroup;

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      auth_provider: ['teacher'],
      username: ['', [Validators.required]],
      password: ['',Validators.required]
    });
  }

  login(): void {
    if(this.validateForm.valid){
      this.authenticationService
      .login(this.validateForm.value)
      .pipe(first())
      .subscribe(
        data => {
          if (data.request_status == 1) {
            console.log(data);
            this.isSuccess = true;
            this.sharedService.openSnackBar('Successfully logged in!', 'X',this.isSuccess)
            localStorage.setItem('user_details', JSON.stringify(data.result.user_details));
            localStorage.setItem('session_id', (data.result.user_details.session.id));
            localStorage.setItem('current_session', (data.result.user_details.session.session));
            localStorage.setItem('school_name', (data.result.user_details.school_name));
            localStorage.setItem('school_attendence_type', (data.result.user_details.school_attendence_type));
            localStorage.setItem('TeacherImage', (data.result.user_details.image));
         
            localStorage.setItem('token', (data.result.token));
            this.router.navigate(['/']);
          }
          else{
            this.isSuccess = false;
            this.sharedService.openSnackBar('unable to login with provided credentials','X',this.isSuccess);
            
          }
        },
        error => {
          console.log(error);
          this.isSuccess = false;
          this.sharedService.openSnackBar('error occured ! Please try later.','X',this.isSuccess);
        }
      );
    }
    
    else
    {
      this.sharedService.markFormGroupTouched(this.validateForm);
    }
  }
   
  
  applyCss(form:FormGroup, field:string)
  {
    return this.sharedService.displayFieldCss(form,field);
  }
  validField(form:FormGroup,field:string)
  {
    return this.sharedService.isFieldValid(form,field);
  }
}
