import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { DashboardService } from 'src/app/services/dashboard.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private service:DashboardService) { }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    margin:10,
    navText : ['<span class="fa-stack"><i class="fa fa-caret-left"></i></span>','<span class="fa-stack"><i class="fa fa-caret-right"></i></span>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }

  carouselElements = [
    {
      title:'Profile',
      link:'profile',
      class:'profile',
      image:'profile-icon.png',
      badge:false,
      count:0
    },
    {
      title:'Announcements',
      link:'announcements',
      class:'announcements',
      image:'announcements-icon.png',
      badge:true,
      count:'*'
    },
    {
      title:'Timetable',
      link:'timetable',
      class:'timetable',
      image:'timetable-icon.png',
      badge:false,
      count:12
    },
    {
      title:'Lectures',
      link:'lectures',
      class:'lectures',
      image:'lectures-icon.png',
      badge:false,
      count:12
    },
    {
      title:'Student',
      link:'student',
      class:'teachers',
      image:'student-icon.png',
      badge:false,
      count:12
    },
    {
      title:'Student Attendance',
      link:'student-attendance',
      class:'attendance',
      image:'attendance-icon.png',
      badge:false,
      count:12
    },
    {
      title:'Group Discussions',
      link:'group-discussions',
      class:'group-discussions',
      image:'group-discussions-icon.png',
      badge:false,
      count:12
    },
    {
      title:'Answer Sheets',
      link:'answer-sheets',
      class:'exams',
      image:'exams-icon.png',
      badge:false,
      count:12
    },
    {
      title:'Grade Cards',
      link:'grade-cards',
      class:'grades',
      image:'grades-icon.png',
      badge:false,
      count:12
    },
    {
      title:'Holiday Calender',
      link:'holiday-calender',
      class:'holiday-calender',
      image:'holiday-calender-icon.png',
      badge:false,
      count:12
    }
  ];

  data = null;

  ngOnInit(): void {
    this.teacher_dashboard_details();
    this.getWeather();
    
    setInterval(function(){ 
      var day = new Date().toLocaleString("en-US", {
        weekday: 'long'
    });
      var today = new Date().toLocaleString("en-US", {
        
        hour: '2-digit',
        minute: '2-digit',
        hour12: true
    });
    let location = document.getElementById("fday");
    location.innerHTML =day+', '+today;
  }, 600000);
  }

  teacher_dashboard_details(): void {
    this.service.teacher_dashboard_details().subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.data = data.results;
          this.carouselElements[1].count = this.data.announcement_count;
          console.log(data);
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  getWeather() {
		let temperature = document.getElementById("temperature");
    
		let api = "https://api.openweathermap.org/data/2.5/weather";
		let apiKey = "f89d5dcfa990b82d0222ed62bfe80297";
	 	navigator.geolocation.getCurrentPosition(success, error);
		function success(position) {
   var latitude = position.coords.latitude;
   var longitude = position.coords.longitude;
	
	

    let url =
      api +
      "?lat=" +
      latitude +
      "&lon=" +
      longitude +
      "&appid=" +
      apiKey +
      "&units=imperial";

	
	
    fetch(url)
      .then(response => response.json())
      .then(data => {
        console.log(data);
        let temp = parseFloat(data.main.temp);
		let tempinc=((temp-32)*5/9).toFixed(0);
        temperature.innerHTML = tempinc + "° C";
      });
     
  }

  function error() {
    //location.innerHTML = "Unable to retrieve your location";
  }
	}
 
 
//

}
