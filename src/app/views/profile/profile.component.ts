import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/services/profile.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(
    private service:ProfileService,
    private fb: FormBuilder, 
    private auth:AuthenticationService, 
    private router:Router,
    private sharedService:SharedService) { }

  data = null;

  passwordChangeStatus = null;
  passwordChangeMessage = null;
  allCountry:any;
  Countryname:any;
  allStates: any;
  Statename:any;
  allCity:any;
  Cityname:any;

  passwordForm!: FormGroup;

  ngOnInit(): void {
    this.buildForm();
    this.teacher_profile_details();
  }

  buildForm(){
    this.passwordForm = this.fb.group({
      old_password: [,[Validators.required]],
      new_password: [, [Validators.required]],
      auth_provider:['school']
    });
  }

  displayStyle = "none";
  
  openPopup() {
    this.displayStyle = "block";
  }
  closePopup() {
    this.displayStyle = "none";
  }

  teacher_profile_details(): void {
    this.service.teacher_profile_details().subscribe({
      next: (data) => {
        console.log(data);
        if(data.request_status == 1){
          this.data = data.results;
          
          this.service.getCountryService().subscribe(res => {
            this.allCountry = res['result'];     
          this.Countryname = this.allCountry.find(e => e.id == this.data.country_code).name;
          
      
          }, err => {
            console.log(err);
          });
          this.service.getStateService(this.data.country_code).subscribe(res => {
            this.allStates = res['result'];
            // console.log(this.allStates);
      
            this.Statename=this.allStates.find(e=>e.id==this.data.state_code).name;
      
          }, err => {
            console.log(err);
          });
          this.service.getCityService(this.data.state_code).subscribe(data => {
            this.allCity = data['result'];
            // console.log(this.allCities);
            this.Cityname=this.allCity.find(e=>e.id==this.data.city_code).name;

          }, err => {
            console.log(err);
          })
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  editBtn_click(){
    let obj = {
      address1: this.data.address1,
      address2: this.data.address2,
      phone: this.data.phone,
      country_code: this.data.country_code,
      state_code: this.data.state_code,
      city_code: this.data.city_code,
      pin_code: this.data.pin_code,
    }

    this.service.sendAddress(obj);
    this.router.navigate(['/edit_profile']);
  }
  changePassword(): void {
    if(this.passwordForm.valid){
      this.auth.changePassword(this.passwordForm.value).subscribe({
        next: (data) => {
          if(data.request_status == 1){
            this.passwordChangeStatus = data.request_status;
            this.passwordChangeMessage = data.result.msg;
          }
        },
        error: (error) => {
          this.passwordChangeStatus = error.error.request_status;
          this.passwordChangeMessage = error.error.result.msg;
        },
        complete: () => {
          this.buildForm();
        }
      });
    }
    else{
      this.sharedService.markFormGroupTouched(this.passwordForm);
    }
  }

  goBack(): void {
    this.router.navigate(['/']);
  }

  applyCss(form:FormGroup, field:string)
  {
    return this.sharedService.displayFieldCss(form,field);
  }
  validField(form:FormGroup,field:string)
  {
    return this.sharedService.isFieldValid(form,field);
  }

}
