import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Component({
  selector: 'app-lectures',
  templateUrl: './lectures.component.html',
  styleUrls: ['./lectures.component.scss']
})
export class LecturesComponent implements OnInit {
  alllecture:any;
  lectureForm!: FormGroup;
  allClass = new Array();
  allSection = new Array();
  allSubjects = new Array();
  isSuccess:boolean = false;
  filepath:any;
  lecturefile :any;
  filen:'Choose File';
  public ContentTypeProfileBanner: any;
  session = localStorage.getItem('session_id');
  constructor(private fb: FormBuilder,private router: Router, private shared: SharedService) { }

  ngOnInit(): void {
    console.log(localStorage.getItem('token'));
    this.buildForm();
    this.getlecture();
    this.getAllClass();
  }
  buildForm(): void {
    this.lectureForm = this.fb.group({
      lecture_title: [null,[Validators.required]],
      lecture_type: ['',[Validators.required]],
      scl_class: ['',[Validators.required]],
      cls_section: ['',[Validators.required]],
      subject: ['',[Validators.required]],
      external_link: [''],
      file_path: ['']
      
    });
  }
  getlecture(){
    
    const params ={
      page_size:0,
      }
      this.shared.teacher_lecture(params).subscribe({
        next: (data) => {
          console.log(data);
         this.alllecture = data.results;
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
  }
  getAllClass() {
    this.lectureForm.controls['cls_section'].setValue('');
    let params = {
      // page_size: '100',
      // page: '1',
      // session: 3,
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          console.log(data);
          this.allClass = data['result'];
          console.log(this.allClass);
          
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  getAllSections() {
    this.lectureForm.controls['cls_section'].setValue('');
    let params = {
      // page_size: '100',
      // page: '1',
      session: localStorage.getItem('session_id'),
      class: this.lectureForm.value.scl_class
    }
    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSection = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  getAllSubjects() {
    this.lectureForm.controls['subject'].setValue('');
    let params = {
      // page_size: '100',
      // page: '1',
      scl_class: this.lectureForm.value.scl_class,
    }
    return this.shared.school_subject_class_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          console.log(data);
          this.allSubjects = data['results'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  classChange(e) { 
	
    this.getAllSections();
    this.getAllSubjects();
  }
  selectFile(event) {
    console.log(event.target.files[0].type);
   
   if (event.target.files && event.target.files[0]) {
    this.lectureForm.patchValue({
      file_path: event.target.files[0]
     })
      //  var filesAmount = event.target.files.length;
      //  for (let i = 0; i < filesAmount; i++) {
      //    var reader = new FileReader();
      //    reader.onload = (event) => {
      //      //console.log(reader.result);
      //    }
      //    reader.readAsDataURL(event.target.files[i]);
      //    this.lectureForm.patchValue({
      //     file_path: event.target.files[0]
      //    })
      //    this.filen = event.target.files[0].name;
      //  }
      this.filen = event.target.files[0].name;
     } else {
       this.filen = 'Choose File';
     }
  
   
 }
  addLectureFile(event: any) {
    const reader = new FileReader();
    if (event.target.files.length) {
      this.ContentTypeProfileBanner = event.target.files[0];
      
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      // this.fileType = this.ProfilePic.name.split('.').pop();
      // if (this.f.media_type.value === '1') {
      // if (
      //   this.ContentTypeProfileBanner.type === 'image/jpg' ||
      //   this.ContentTypeProfileBanner.type === 'image/jpeg' ||
      //   this.ContentTypeProfileBanner.type === 'image/png' ||
      //   this.ContentTypeProfileBanner.type === 'image/gif'
      // ) {
      //   // this.formControlsGroup1.profilePic.setValue(this.ContentTypeProfileBanner)
      //   console.log(this.ContentTypeProfileBanner);


      // } else {
      //   this.ContentTypeProfileBanner = null;
      //   // this.f.advertisement.setValue(null);
      //   this.ContentTypeProfileBanner = '';
      //   // this.previewUrl = '';
      //   this.shared.openSnackBar('Only Image file is accepted', 'X');
      // }
    }
  }
  submitForm(): void {
   
      console.log(this.lectureForm.value);
      if(this.lectureForm.valid){
        if (this.ContentTypeProfileBanner) {
          this.filepath = this.ContentTypeProfileBanner;
        } else {
          this.filepath='';
        }
        let formData = new FormData();
        formData.append('lecture_title', this.lectureForm.value.lecture_title);
       formData.append('lecture_type', this.lectureForm.value.lecture_type);
        formData.append('external_link', '');
        //formData.append('question_type', this.lectureForm.value.question_type);
        formData.append('scl_class', this.lectureForm.value.scl_class);
        formData.append('cls_section', this.lectureForm.value.cls_section);
        formData.append('subject', this.lectureForm.value.subject);
        // if (this.lectureForm.value.file_path){
        // formData.append('file_path', this.lectureForm.value.file_path, this.lectureForm.value.file_path['name']);
        // } else {
        //   formData.append('file_path','');
        // }
        if (this.lectureForm.value.file_path){
          formData.append('file_path', this.lectureForm.value.file_path, this.lectureForm.value.file_path['name']);
        }
        // let body ={
        //   lecture_title:this.lectureForm.value.lecture_title,
        //   lecture_type:this.lectureForm.value.lecture_type,
        //   external_link:this.lectureForm.value.external_link, 
        //   question_type:this.lectureForm.value.question_type, 
        //   scl_class:this.lectureForm.value.scl_class, 
        //   cls_section:this.lectureForm.value.cls_section, 
        //   subject:this.lectureForm.value.subject, 
        //   file_path:this.filepath,    
        // }
        console.log(formData);

      this.shared.teacher_lecture_post(formData).subscribe({
        next: (data) => {
          console.log(data);
          if(data.request_status == 1){
            this.isSuccess = true;
            this.shared.openSnackBar('Lecture Added Successfully', 'X',this.isSuccess);
            this.getlecture();
            this.lectureForm.reset();
          }
        
          
        },
        error: (error) => {
          
          this.isSuccess = false;
            this.shared.openSnackBar(error, 'X',this.isSuccess)
        },
        complete: () => {
          // console.info('complete')
        }
      });
      } else {
        this.isSuccess = false;
        this.shared.openSnackBar('Please Fill All the fields', 'X',this.isSuccess)
      }
      // let formData = new FormData();
      //   formData.append('lecture_title', this.lectureForm.value.lecture_title);
      //  formData.append('lecture_type', this.lectureForm.value.lecture_type);
      //   formData.append('external_link', this.lectureForm.value.external_link);
      //   formData.append('question_type', this.lectureForm.value.question_type);
      //   formData.append('scl_class', this.lectureForm.value.scl_class);
      //   formData.append('cls_section', this.lectureForm.value.cls_section);
      //   formData.append('subject', this.lectureForm.value.subject);
      //   if (this.ContentTypeProfileBanner) {
      //     formData.append('file_path',this.ContentTypeProfileBanner);
      //   } else {
      //     formData.append('file_path','');
      //   }
        
  }

}
