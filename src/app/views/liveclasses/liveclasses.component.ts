import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LiveclassesService } from 'src/app/services/liveclasses.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-liveclasses',
  templateUrl: './liveclasses.component.html',
  styleUrls: ['./liveclasses.component.scss']
})
export class LiveclassesComponent implements OnInit {

  searchForm:FormGroup;
  currentSession = localStorage.getItem("session_id").toString();

  minDate;
  startTime;
  allClassList=[];
  allSectionList = [];
  allRoutineList=[];
  allupcomingLiveClass=[];
  
  // pagination
  page=1;
  pageSize=10;
  totalItems;
  constructor(
    private router:Router,
    private shared:SharedService,
    private liveClassService:LiveclassesService,
    private fb:FormBuilder
  ) {
    this.searchForm = fb.group({
      search_class:[''],
      search_section:[''],
      search_date:[''],
      search_routine:['']
    })
  }

  ngOnInit(): void {
    this.minDate = this.deletePreviousDates();
    this.getAllClass();
    this.getAllLiveClass();
  }
  
  goBack(){
    this.router.navigate(['/']);
  }

  getAllClass() {
    let params = {
      page_size: '',
      // page: '1',
      session: this.currentSession,
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allClassList = data['result'];
          // console.log(this.allClassList);
        }
      },
      error: (error) => {
        console.error(error);
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  onChangeClass(e){
    let classId = e.target.value;
    this.getAllSections(classId);
  }

  getAllSections(classId) {
    let params = {
      // page_size: '100',
      // page: '1',
      session: this.currentSession,
      class: classId
    }
    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSectionList = data['result'];
          // console.log(this.allSectionList);
        }
      },
      error: (error) => {
        console.error(error);
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  
  onChangeDate(e){
    let date = e.target.value;
    let dayId = new Date(date).getDay();
    let dayName = this.shared.allWeekDays().find(elem => elem.id == dayId)['day'];
    // console.log(dayName);
    this.getAllRoutineList(dayName);
  }

  getAllRoutineList(day){
    let params = {
      scl_class:this.searchForm.get('search_class').value,
      cls_section:this.searchForm.get('search_section').value,
      day
    }
    this.liveClassService.getRoutineDaywise(params).subscribe(data =>{
      console.log(data);
      if(data.request_status == 1){
        this.allRoutineList = data['result'];
      }
    }, err =>{
      console.log(err);
    })
  }

  onSelectRoutine(e){
    if(this.allRoutineList.length != 0){
      // console.log(this.allRoutineList);
      this.startTime = this.allRoutineList.find(elem => elem.id == e.target.value)['start_time']
    }
  }
  
  
  searchBtnClick(){
    let params ={
      page_size:'100',
      scl_class:this.searchForm.get('search_class')?.value,
      cls_section: this.searchForm.get('search_section')?.value,
      date: this.searchForm.get('search_date')?.value,
      
    }
    this.liveClassService.getLiveClass(params).subscribe(data =>{
      if(data['results'].length != 0){
        this.shared.openSnackBar("Data found!","X",true);
        data['results'].forEach( elem =>{
          let startTime = elem.routine.start_time.substr(0,5);
          let endtime = elem.routine.end_time.substr(0,5);
          elem.routine.start_time = startTime;
          elem.routine.end_time = endtime;
        })
        this.allupcomingLiveClass = data['results'].filter(elem => elem.is_upcoming == true);
        console.log(this.allupcomingLiveClass);
        this.totalItems = this.allupcomingLiveClass.length;
        this.searchForm.reset();
      }
      else{
        this.allupcomingLiveClass = data['results'];
        this.shared.openSnackBar("No results found!","X",false);
      } 
    }, err =>{
      console.log(err);
    })
  }

  getAllLiveClass(){
    let params = {
      page_size:'100'
    }
    this.liveClassService.getLiveClass(params).subscribe(data =>{
      data['results'].forEach( elem =>{
        let startTime = elem.routine.start_time.substr(0,5);
        let endtime = elem.routine.end_time.substr(0,5);
        elem.routine.start_time = startTime;
        elem.routine.end_time = endtime;
      })
      this.allupcomingLiveClass = data['results'].filter(elem => elem.is_upcoming == true);
      this.totalItems = this.allupcomingLiveClass.length;
      console.log(this.allupcomingLiveClass);
    }, err=>{
      this.shared.openSnackBar("Error occured. Try after some time!","X", false);
      console.log(err);
    })
  }

  startcallbtn(liveclass){
    let classlink = liveclass.class_link;
    console.log(classlink);
    window.open(classlink,"_blank");
  }

  deletePreviousDates(){
    var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        var mainMonth = '0' + month.toString();
    if(day < 10)
        var mainday = '0' + day.toString();
    var minDate= year + '-' + mainMonth + '-' + mainday;
    return minDate;
  }
  
  handlePageChange(event){
    this.page = event;
  }


  applyCss(form:FormGroup, field:string)
  {
    return this.shared.displayFieldCss(form,field);
  }
  validField(form:FormGroup,field:string)
  {
    return this.shared.isFieldValid(form,field);
  }
}
