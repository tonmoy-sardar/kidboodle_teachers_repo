import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LiveclassesService } from 'src/app/services/liveclasses.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-liveclasshistory',
  templateUrl: './liveclasshistory.component.html',
  styleUrls: ['./liveclasshistory.component.scss']
})
export class LiveclasshistoryComponent implements OnInit {

  allLiveClass=[];
  allCompletedLiveClass=[];

  // pagination
  page=1;
  pageSize=10;
  totalItems;
  constructor(
    private router:Router,
    private liveClassService:LiveclassesService,
    private shared:SharedService
  ) { }

  ngOnInit(): void {
    this.getAllLiveClass();
  }

  goBack(){
    this.router.navigate(['/'])
  }

  getAllLiveClass(){
    let params = {
      page_size:'100',
      date:"",
      scl_class:"",
      cls_section:"",
      routine:"",

    }
    this.liveClassService.getLiveClass(params).subscribe(data =>{
      console.log(data);
      data['results'].forEach( elem =>{
        let startTime = elem.routine.start_time.substr(0,5);
        let endtime = elem.routine.end_time.substr(0,5);
        elem.routine.start_time = startTime;
        elem.routine.end_time = endtime;
      })
      this.allLiveClass = data['results'];
      this.allCompletedLiveClass = this.allLiveClass.filter(elem => elem.is_upcoming == false);
    
      this.totalItems = this.allCompletedLiveClass.length;
    }, err=>{
      // console.log(err);
      this.shared.openSnackBar('Error occured! Try after some time.','X',false);
    })
  }

  handlePageChange(event){
    this.page = event;
  }

}
