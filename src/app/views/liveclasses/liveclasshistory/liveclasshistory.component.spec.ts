import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveclasshistoryComponent } from './liveclasshistory.component';

describe('LiveclasshistoryComponent', () => {
  let component: LiveclasshistoryComponent;
  let fixture: ComponentFixture<LiveclasshistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveclasshistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveclasshistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
