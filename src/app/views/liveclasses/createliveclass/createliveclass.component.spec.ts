import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateliveclassComponent } from './createliveclass.component';

describe('CreateliveclassComponent', () => {
  let component: CreateliveclassComponent;
  let fixture: ComponentFixture<CreateliveclassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateliveclassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateliveclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
