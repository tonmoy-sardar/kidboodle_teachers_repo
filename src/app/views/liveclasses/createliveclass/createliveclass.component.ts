import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LiveclassesService } from 'src/app/services/liveclasses.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-createliveclass',
  templateUrl: './createliveclass.component.html',
  styleUrls: ['./createliveclass.component.scss']
})
export class CreateliveclassComponent implements OnInit {
  liveClassForm:FormGroup;
  currentSession = localStorage.getItem("session_id")
  allClassList=[];
  allSectionList=[];
  allRoutineList=[];

  constructor(
    private router:Router,
    private fb:FormBuilder,
    private liveClassService:LiveclassesService,
    private shared:SharedService
  ) { 
    this.liveClassForm = fb.group({
      class:['',Validators.required],
      section:[''],
      date:['',Validators.required],
      routine:['',Validators.required],
      zoomlink:['',Validators.required]
    })
  }

  ngOnInit(): void {
    this.getAllClass();
  }

  goBack(){
    this.router.navigate(['/']);
  }
  getAllClass() {
    let params = {
      page_size: '100',
      session: this.currentSession,
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allClassList = data['result'];
          // console.log(this.allClassList);
        }
      },
      error: (error) => {
        this.shared.openSnackBar("Cannot fetch all class!","X",false);
        console.error(error);
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  getAllSections(classId) {
    let params = {
      // page_size: '100',
      // page: '1',
      session: this.currentSession,
      class: classId
    }
    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSectionList = data['result'];
          // console.log(this.allSectionList);
        }
      },
      error: (error) => {
        this.shared.openSnackBar("Cannot fetch sections","X",false);
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  onChangeClass(e){
    let classId = e.target.value;
    this.getAllSections(classId);
  }
  onChangeDate(e){
    let date = e.target.value;
    let dayId = new Date(date).getDay();
    let dayName = this.shared.allWeekDays().find(elem => elem.id == dayId)['day'];
    console.log(dayName);
    this.getAllRoutineList(dayName);
  }
  getAllRoutineList(day){
    let params = {
      scl_class: this.liveClassForm.get('class').value,
      cls_section: this.liveClassForm.get('section').value,
      day
    }
    // console.log(params);
    this.liveClassService.getRoutineDaywise(params).subscribe(data =>{
      console.log(data);
      if(data.request_status == 1){
        this.allRoutineList = data['result'];
      }
    }, err =>{
      this.shared.openSnackBar("Cannot get all routine","X", false);
      console.log(err);
    })
  }

  createBtnClick(){
    if(this.liveClassForm.valid){
      let body = {
        scl_class: this.liveClassForm.get('class')?.value,
        cls_section: this.liveClassForm.get('section')?.value,
        routine: this.liveClassForm.get('routine')?.value,
        date: this.liveClassForm.get('date').value,
        class_link: this.liveClassForm.get('zoomlink').value,
        // is_active: false
      }
      this.liveClassService.createLiveClass(body).subscribe(data =>{
        console.log(data);
        this.shared.openSnackBar('Added successfully','X',true);
        this.router.navigate(['/live-classes']);
      }, err =>{
        console.log(err);
      })
    }
    else{
      this.shared.openSnackBar('All the fields are required!','X', false);
      this.shared.markFormGroupTouched(this.liveClassForm);
    }
  }

  applyCss(form:FormGroup, field:string)
  {
    return this.shared.displayFieldCss(form,field);
  }
  validField(form:FormGroup,field:string)
  {
    return this.shared.isFieldValid(form,field);
  }

}
