import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AnnouncementsService } from '../../services/announcements.service';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.scss']
})
export class AnnouncementsComponent implements OnInit {

  constructor(
    private service:AnnouncementsService,
    private router: Router,
    private fb: FormBuilder,
    private shared:SharedService,
    private sharedService:SharedService) { }

  announcementForm!: FormGroup;

  data = null;
  selectedTab = "CREATE";
  pageSize = 10;
  pageNumber = 1;
  numberOfPages = 0;
  classSectionData = {};
  file1 = null;
  file2 = null;
  file3 = null;

  allClass = new Array();
  allSection = new Array();
  allstudent = new Array();
  ngOnInit(): void {
    this.teacher_all_announcement_list_get();
    this.buildForm();
    this.getAllClass();
  }

  buildForm(): void {
    this.announcementForm = this.fb.group({
      title: [null,[Validators.required]],
      description: [null, [Validators.required]],
      date: ['', [Validators.required]],
      to_school_or_class: [1],
      scl_class: ['',[Validators.required]],
      cls_section: ['',[Validators.required]],
      image1:[null],
      image2:[null],
      image3:[null]
    });
  }

  changeSelectedTab(tab): void {
    this.selectedTab = tab;
  }

  changePage(moveToPage): void {
    if(moveToPage === 'NEXT'){
      this.pageNumber++;
    }else if(moveToPage === 'PREVIOUS'){
      if(this.pageNumber !== 1){
        this.pageNumber--;
      }
    }else{
      this.pageNumber = moveToPage;
    }
    this.teacher_all_announcement_list_get();
  }

  teacher_all_announcement_list_get(): void {
    this.service.teacher_all_announcement_list_get(this.pageNumber,this.pageSize).subscribe({
      next: (data) => {
        if(data.request_status == 1){
         this.data = data.results;
         this.numberOfPages = Math.ceil(data.count/this.pageSize);
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  getAllClass() {
    this.announcementForm.controls['cls_section'].setValue("");
    let params = {
      page_size: '100',
      page: '1',
      session: 3,
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allClass = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  getAllSections() {
    let params = {
      page_size: '100',
      page: '1',
      session: localStorage.getItem('session_id'),
      class: this.announcementForm.value.scl_class
    }
    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSection = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  }
  //http://3.17.248.213:8002/teacher_student_details_get/?page_size=1&class=22&section=105&session=13
  getAllStudent(){
    let params = {
      page_size: '100',
      section:this.announcementForm.value.cls_section,
      session: localStorage.getItem('session_id'),
      class: this.announcementForm.value.scl_class
    }
    return this.shared.school_class_section_student_list(params).subscribe({
      next: (data) => {
        console.log(data);
        if(data.request_status == 1){
          this.allstudent = data.results;
          console.log(this.allstudent);
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  classChange(e) { 
    this.getAllSections();
  }

  routeToAnnoncementDetailsPage(id){
    this.router.navigate(['announcements/details/'+id])
  }

  selectFile(event,image) {
   
   if (event.target.files[0].type == "image/png" || event.target.files[0].type == "image/jpeg" || event.target.files[0].type == "image/jpg") {
     if (event.target.files && event.target.files[0]) {
       var filesAmount = event.target.files.length;
       for (let i = 0; i < filesAmount; i++) {
         var reader = new FileReader();
         reader.onload = (event) => {
           //console.log(reader.result);
         }
         reader.readAsDataURL(event.target.files[i]);

         if(image === 1){
          this.announcementForm.patchValue({
            image1: event.target.files[0]
           })
           this.file1 = event.target.files[0].name;
         }else if(image === 2){
          this.announcementForm.patchValue({
            image2: event.target.files[0]
           })
           this.file2 = event.target.files[0].name;
         }else{
          this.announcementForm.patchValue({
            image3: event.target.files[0]
           })
           this.file3 = event.target.files[0].name;
         }
       }
     }
   } else {
     this.file1 = "Choose file";
     alert("Please select png / jpeg / jpg");
   }
 }

  submitForm(): void {
   if(this.announcementForm.valid){
    var formValue = new FormData();

    formValue.append('title', this.announcementForm.value.title);
    formValue.append('description', this.announcementForm.value.description);
    formValue.append('date', this.announcementForm.value.date);
    formValue.append('to_school_or_class', this.announcementForm.value.to_school_or_class);
    formValue.append('scl_class', this.announcementForm.value.scl_class);
    formValue.append('cls_section', this.announcementForm.value.cls_section);

    if (this.announcementForm.value.image1){
      formValue.append('image1', this.announcementForm.value.image1, this.announcementForm.value.image1['name']);
    }
    else{
        formValue.append('image1', ''  );
    }

    if (this.announcementForm.value.image2){
      formValue.append('image2', this.announcementForm.value.image2, this.announcementForm.value.image2['name']);
    }
    else{
        formValue.append('image2', ''  );
    }

    if (this.announcementForm.value.image3){
      formValue.append('image3', this.announcementForm.value.image3, this.announcementForm.value.image3['name']);
    }
    else{
        formValue.append('image3', ''  );
    }
    // formValue.append('school',this.userForm.value.school);

  this.service.teacher_announcement_post(formValue).subscribe({
    next: (data) => {
      if(data.request_status == 1){
       this.routeToAnnoncementDetailsPage(data.results.id);
      }
    },
    error: (error) => {
      console.error(error)
    },
    complete: () => {
      // console.info('complete')
    }
  });
   }
   else{
     this.sharedService.markFormGroupTouched(this.announcementForm);
   }
  }

  applyCss(form:FormGroup, field:string)
  {
    return this.sharedService.displayFieldCss(form,field);
  }
  validField(form:FormGroup,field:string)
  {
    return this.sharedService.isFieldValid(form,field);
  }

  goBack(): void {
    this.router.navigate(['/']);
  }
}
