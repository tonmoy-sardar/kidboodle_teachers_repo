import { Component, OnInit } from '@angular/core';
import { AnnouncementsService } from '../../../services/announcements.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
@Component({
  selector: 'app-announcements-details',
  templateUrl: './announcements-details.component.html',
  styleUrls: ['./announcements-details.component.scss']
})
export class AnnouncementsDetailsComponent implements OnInit {

  constructor(private service:AnnouncementsService,private route: ActivatedRoute, private router: Router) { }

  data = null;
  currentId = null;

  ngOnInit(): void {
    this.currentId = this.route.snapshot.paramMap.get('id');
    this.getAnnouncementData();
  }

  getAnnouncementData(): void {
    this.service.teacher_all_announcement_list_one_get(this.currentId).subscribe({
      next: (data) => {
        if(data.request_status == 1){
         this.data = data.results[0];
         console.log(data);
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  
  goBack(): void {
    this.router.navigate(['/announcements']);
  }

}
