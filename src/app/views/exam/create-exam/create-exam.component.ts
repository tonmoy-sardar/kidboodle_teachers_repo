import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SharedService } from '../../../services/shared.service';
import { HomeworksService } from '../../../services/homeworks.service';

@Component({
  selector: 'app-create-exam',
  templateUrl: './create-exam.component.html',
  styleUrls: ['./create-exam.component.scss']
})
export class CreateExamComponent implements OnInit {

  constructor(private service:HomeworksService, private fb: FormBuilder,private router: Router,private shared:SharedService) { }

  homeworkForm!: FormGroup;
  allClass = new Array();
  allSection = new Array();
  allExam = new Array();
  allschedule = new Array();
  // allSubjects = new Array();
  allroutine = new Array();
  public fileDiv = false;
  public onlineDiv = false;
  snackBar: any;
  public ContentTypeProfileBanner: any;
  
  routineObj = {};
  ngOnInit(): void {
    this.buildForm();
    this.getAllClass();
  }

  
  buildForm(): void {
    this.homeworkForm = this.fb.group({
      title: [null,[Validators.required]],
      description: [null,[Validators.required]],
      // start_date: [null,[Validators.required]],
      // end_date: [null,[Validators.required]],
      question_type: ['',[Validators.required]],
      scl_class: ['',[Validators.required]],
      cls_section: ['',[Validators.required]],
      attachement: ['',[]],
      examType: ['',[Validators.required]],
      attachment_link: [null,[Validators.required]],
      schedule: ['',[Validators.required]],
      routine: ['',[Validators.required]],
    });
  }

  getAllClass() {
    this.homeworkForm.controls['cls_section'].setValue('');
    let params = {
      page_size: '100',
      page: '1',
      session: localStorage.getItem("session_id"),
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allClass = data['result'];
          this.getAllExam()
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
      
    });
   
  }

  getAllSections() {
    
    this.homeworkForm.controls['cls_section'].setValue('');
    let params = {
      page_size: '100',
      page: '1',
      session: localStorage.getItem("session_id"),
      class: this.homeworkForm.value.scl_class
    }
    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSection = data['result'];
          console.log( this.allSection,'section');
          
      
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
    
  };

  classChange(e) { 
    this.getAllSections();
    this.getAllExam()
    // this.getAllSubjects();
  }
  secionChange(e){
    this.getAllExam()
  }

  secionExam(e){
this.schedule()
  }
  secionSchedule(e){
this.routineList()
  }

  
  getAllExam() {
    this.homeworkForm.controls['examType'].setValue('');
    let params = {
      page_size: '100',
      page: '1',
      class: this.homeworkForm.value.scl_class,
      session: this.homeworkForm.value.cls_section,
    }
    console.log(params,'Class,Section');
    
    return this.shared.teacher_exam_type_list(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allExam = data['result'];
          console.log(this.allExam);
          
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };
  // http://3.17.248.213:8002/teacher_student_exam_schedule_list/?scl_class=1&cls_section=1&exam_type=1

  schedule() {
    this.homeworkForm.controls['schedule'].setValue('');
    let params = {
      page_size: '100',
      page: '1',
      scl_class: this.homeworkForm.value.scl_class,
      cls_section: this.homeworkForm.value.cls_section,
      exam_type: this.homeworkForm.value.examType,
    }
    console.log(params,'Class,Section,exam');
    
    return this.shared.teacher_student_exam_schedule_list(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allschedule = data['result'];
          console.log(this.allschedule,'schedule');
          
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  routineList(){
      this.homeworkForm.controls['routine'].setValue('');
      let params = {
        page_size: '100',
        page: '1',
        scl_class: this.homeworkForm.value.scl_class,
        cls_section: this.homeworkForm.value.cls_section,
        exam_type: this.homeworkForm.value.examType,
        exam_schedule: this.homeworkForm.value.schedule,
      }
      console.log(params,'Class,Section,exam,schedule');
      
      return this.shared.teacher_exam_routine_create(params).subscribe({
        next: (data) => {
          if(data.request_status == 1){
            this.allroutine = data['results'];
            console.log(this.allroutine,'routine');
            
          }
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
      
  
  }
  selectRoutine(e){
    // console.log(e.target.value);
    let routineId = e.target.value;
    if(this.allroutine.length != 0){
      let routine = this.allroutine.find(elem => elem.id == routineId);
      this.routineObj = routine;
    }
  }

  fileAttached(e){
    console.log(e.target.value);
    this.homeworkForm.value.question_type = e.target.value;
    if(e.target.value == 1)
    {
      this.fileDiv = true;
    }
    else
    {
      this.fileDiv = false;
    }
    
     }
   
     addQuestionImage(event: any) {
       console.log(event);
       
       const reader = new FileReader();
       if (event.target.files.length) {
         this.ContentTypeProfileBanner = event.target.files[0];
         console.log(event.target.files[0]);
         const [file] = event.target.files;
         reader.readAsDataURL(file);
         // this.fileType = this.ProfilePic.name.split('.').pop();
         // if (this.f.media_type.value === '1') {
         if (
           this.ContentTypeProfileBanner.type === 'image/jpg' ||
           this.ContentTypeProfileBanner.type === 'image/jpeg' ||
           this.ContentTypeProfileBanner.type === 'image/png' ||
           this.ContentTypeProfileBanner.type === 'image/gif'
         ) {
           // this.formControlsGroup1.profilePic.setValue(this.ContentTypeProfileBanner)
           console.log(this.ContentTypeProfileBanner)
   
   
         } else {
           this.ContentTypeProfileBanner = null;
           // this.f.advertisement.setValue(null);
           this.ContentTypeProfileBanner = '';
           // this.previewUrl = '';
           this.snackBar.open('Only Image file are accepted', 'Close', {
             panelClass: 'error-popup',
           });
         }
       }
     }
   

  submitForm(): void {
    console.log(this.homeworkForm.value);
    // const start_date = this.shared.formatDate(this.homeworkForm.value.start_date);
    //   this.homeworkForm.patchValue({
    //     start_date: start_date
    //   })
    //   const end_date = this.shared.formatDate(this.homeworkForm.value.end_date);
    //   this.homeworkForm.patchValue({
    //     end_date: end_date
    //   })

    // http://3.17.248.213:8002/teacher_exam_post/

      let formData = new FormData();

      formData.append('scl_class', this.homeworkForm.value.scl_class);
      formData.append('cls_section', this.homeworkForm.value.cls_section);
      formData.append('exam_schedule', this.homeworkForm.value.schedule);
      formData.append('title', this.homeworkForm.value.title);
      formData.append('description', this.homeworkForm.value.description);
      formData.append('question_type', this.homeworkForm.value.question_type);
      formData.append('exam_type', this.homeworkForm.value.examType);
      formData.append('exam_routine', this.homeworkForm.value.routine);
      formData.append('Method','');
      
      if (this.ContentTypeProfileBanner) {
        formData.append('attachment_link',this.ContentTypeProfileBanner);
      } else {
        formData.append('attachment_link','');
      }
      this.service.teacher_exam_post(formData).subscribe({
        next: (data) => {
          if(data.request_status == 1){
            localStorage.setItem('examAdd', JSON.stringify(data.results));
            this.routeToAddQuestionsPage();
          }
          console.log(data);
          if (data.results?.question_type == 1) {
            this.goBack()
           
          } else {
            this.routeToAddQuestionsPage();
          }
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
  }

  routeToAddQuestionsPage(){
    this.router.navigate(['exam/create-exam/add-questions']);
  }

  goBack(): void {
    this.router.navigate(['/exams']);
  }

}
