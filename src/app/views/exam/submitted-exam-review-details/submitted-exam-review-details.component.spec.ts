import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittedExamReviewDetailsComponent } from './submitted-exam-review-details.component';

describe('SubmittedExamReviewDetailsComponent', () => {
  let component: SubmittedExamReviewDetailsComponent;
  let fixture: ComponentFixture<SubmittedExamReviewDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubmittedExamReviewDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedExamReviewDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
