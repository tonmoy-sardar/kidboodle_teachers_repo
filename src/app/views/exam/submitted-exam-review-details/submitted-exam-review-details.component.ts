import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { HomeworksService } from '../../../services/homeworks.service';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal, NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-submitted-exam-review-details',
  templateUrl: './submitted-exam-review-details.component.html',
  styleUrls: ['./submitted-exam-review-details.component.scss']
})
export class SubmittedExamReviewDetailsComponent implements OnInit {
exam_ans_id:string='';
exam_id:string="";
student_id:string="";
questionarray:any="";
student_fname:string="";
student_lname:string="";
subject:string="";
answer_type = [];
indexs;
teachers_comment = [];
stu_class:string="";
class_section:string="";
all_ans_list:any;
comments:string;
@ViewChild('remarksModal') remarksModal : TemplateRef<any>; // Note:
  constructor( private modalService: NgbModal ,private _snackBar: MatSnackBar,private service: HomeworksService,private router: Router) { }

  ngOnInit(): void {
    this.exam_ans_id = localStorage.getItem('exam_ans_id');
    this.exam_id = localStorage.getItem('exam_id');
    this.student_id = localStorage.getItem('student_id');
    this.student_fname = localStorage.getItem("student_fname");
    this.student_lname = localStorage.getItem("student_lname");
    this.subject = localStorage.getItem("subject")
    this.stu_class = localStorage.getItem("stu_class");
    this.class_section = localStorage.getItem("class_section");

    this.getquestion();
  }

  goBack(): void {
    this.router.navigate(['/#/exams']);
  }
  
  teacher_remarks(s){
    if(s == ''){
      this.comments = 'No Remarks';
    }else {
      this.comments = s;
    }
    

    this.modalService.open(this.remarksModal,{ size: 'lg', backdrop: 'static' });
  }
  
  getquestion()
  {
    const params = {
      student_id: this.student_id,
      
      exam_ans_id: this.exam_ans_id,
      exam_id: this.exam_id,
    }

  this.service.teacher_submitted_exam_remarks_view(params).subscribe({
    next: (data) => {
      console.log(data);
      this.questionarray = data.results;
      
    },
    error: (error) => {
      console.error(error)
    },
    complete: () => {
      // console.info('complete')
    }
  });
  }

}
