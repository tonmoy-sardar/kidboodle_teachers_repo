import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittedExamDetailsComponent } from './submitted-exam-details.component';

describe('SubmittedDetailsComponent', () => {
  let component: SubmittedExamDetailsComponent;
  let fixture: ComponentFixture<SubmittedExamDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubmittedExamDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedExamDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
