import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { HomeworksService } from '../../../services/homeworks.service';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal, NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-submitted-exam-details',
  templateUrl: './submitted-exam-details.component.html',
  styleUrls: ['./submitted-exam-details.component.scss']
})
export class SubmittedExamDetailsComponent implements OnInit {
exam_ans_id:string='';
exam_id:string="";
student_id:string="";
questionarray:any="";
student_fname:string="";
student_lname:string="";
subject:string="";
answer_type = [];
marks_obtain = [];
indexs;
teachers_comment = [];
stu_class:string="";
class_section:string="";
all_ans_list:any;
@ViewChild('remarksModal') remarksModal : TemplateRef<any>; // Note:
constructor( private modalService: NgbModal ,private _snackBar: MatSnackBar,private service: HomeworksService,private router: Router) { }

  ngOnInit(): void {
    this.exam_ans_id = localStorage.getItem('exam_ans_id');
    this.exam_id = localStorage.getItem('exam_id');
    this.student_id = localStorage.getItem('student_id');
    this.student_fname = localStorage.getItem("student_fname");
    this.student_lname = localStorage.getItem("student_lname");
    this.subject = localStorage.getItem("subject")
    this.stu_class = localStorage.getItem("stu_class");
    this.class_section = localStorage.getItem("class_section");
    this.getquestion();
  }

  goBack(): void {
    this.router.navigate(['/exam']);
  }
 marks_change(marks,ind){
   let mark = marks.target.value;
   this.marks_obtain.splice(ind, 1, mark);
 }
 
  teacher_remarks(s){
    this.indexs = s;
    this.modalService.open(this.remarksModal,{ size: 'lg', backdrop: 'static' });
  }
  remarks_add(){
    var rem = (<HTMLInputElement>document.getElementById('tremarks')).value;
    (<HTMLInputElement>document.getElementById('tremarks')).value = '';
    this.teachers_comment.splice(this.indexs, 1, rem);
    
    document.getElementById("modal_close").click();
    this._snackBar.open("Remarks Added Successfully",'close', {
      duration: 5000,
   });
  }
  getquestion()
  {
    const params = {
      student_id: this.student_id,
      exam_ans_id: this.exam_ans_id,
      exam_id: this.exam_id,
    }

  this.service.teacher_submitted_exam_details_view(params).subscribe({
    next: (data) => {
      console.log(data);
      this.questionarray = data.results;
      for (var i = 0; i < this.questionarray.length; i++) {
        this.teachers_comment[i] = '';
        this.marks_obtain[i] = '';
      }
    },
    error: (error) => {
      console.error(error)
    },
    complete: () => {
      // console.info('complete')
    }
  });
  }
  onSubmit(f: NgForm){
    if (f.valid == false) {
      
        this._snackBar.open("Please Fillup all fields",'close', {
          duration: 5000,
      });
    } else {
      this.all_ans_list= [];
      for (var i = 0; i < this.questionarray.length; i++) {

          var o = {
            exam_ans_det_id: this.questionarray[i].id,
            comment:this.teachers_comment[i],
            marks_obtained:this.marks_obtain[i],
            is_right:this.answer_type[i]
          };
          this.all_ans_list.push(o);  
      }
      console.log(this.all_ans_list);
      let body ={
        all_ans_list:this.all_ans_list,
      }
      const params = {
        exam_id: this.exam_id,
        method: 'edit',
      }
      this.service.teacher_online_exam_remarks_submit(body,params).subscribe({
        next: (data) => {
          this._snackBar.open("Remarks Submitted Successfully",'close', {
            duration: 5000,
         });
         localStorage.setItem("remarks_submitted",'success');
         this.router.navigate(['/#/exams']);
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
    }
  }
}
