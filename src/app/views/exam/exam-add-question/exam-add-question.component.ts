import { Component, OnInit } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SharedService } from '../../../services/shared.service';
import { HomeworksService } from 'src/app/services/homeworks.service';

@Component({
  selector: 'app-exam-add-question',
  templateUrl: './exam-add-question.component.html',
  styleUrls: ['./exam-add-question.component.scss']
})
export class ExamAddQuestionComponent implements OnInit {


  constructor(private service:HomeworksService,private router: Router,private shared:SharedService) {}

  question_data = null;
  questionCondition : any;

  mainForm = {
    homework:'1',
    question: null,
    media_type: "3",
    question_type: "SELECT",
    is_multiple: '0',
    media:null,
    media_name:'',
    options_list: [],
  };
  currentOption = null;

  editMode = false;
  currentIndex = null;

  finalQuestions = [];

  ngOnInit(): void {
    this.question_data = JSON.parse(localStorage.getItem('examAdd'));
    console.log( this.question_data);
    this.questionList()
  }

  questionList(){
    const params = {
      page_size: '100',
      page: '1',
      exam_id: this.question_data.id,
    }
    
    this.service.teacher_exam_question_details(params).subscribe({
      next: (data) => {
        console.log(data)
        if(data.request_status == 1){
        console.log(data,'examList')
        this. finalQuestions = data.results
        console.log(this. finalQuestions);
        
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });

  }
  addQuestions(): void {
    this.submitForm();
    // if(this.editMode){
    //   this.finalQuestions[this.currentIndex] = this.mainForm;
    //   this.editMode = false;
    //   this.currentIndex = null;
    // }else{
    //   this.finalQuestions.push(this.mainForm);
    // }
    this.questionList()

    this.resetMainForm();
  }

  editQuestion(index): void {
    this.mainForm = this.finalQuestions[index];
    this.editMode = true;
    this.currentIndex = index;
  }

  deleteQuestion(index): void {
    // this.finalQuestions.splice(index,1)
    console.log(index);
   const data =  {
      method : 'delete',
      question_id : index
    }
    this.service.questionDeleted(data).subscribe({
      next: (data) => {
        console.log(data)
        if(data.request_status == 1){
        console.log(data)
        alert('Question is deleted successfully')
        this.questionList();
      
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  }

  addOptions(): void {
    if(this.mainForm.options_list.length < 4){
      this.mainForm.options_list.push(this.currentOption);
      this.currentOption = null;
    }

  }

  deleteOptions(index): void {
    this.mainForm.options_list.splice(index,1);
  }

  selectFile(event) {
   
      if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
          var reader = new FileReader();
          reader.onload = (event) => {
            //console.log(reader.result);
          }
          reader.readAsDataURL(event.target.files[i]);
           this.mainForm.media = event.target.files[0];  
           this.mainForm.media_name = this.mainForm.media['name'];  
        }
      }
  }

  resetMainForm(): void {
    this.mainForm = {
      homework:'1',
      question: null,
      media_type: "3",
      question_type: "SELECT",
      is_multiple: '0',
      media:null,
      media_name:'',
      options_list: [],
    };
    this.currentOption = null;
  }
  questionType(e){
  console.log(e.target.value);
  if(e.target.value === '1'){
    this.questionCondition = 1;
  }
  else if(e.target.value === '2'){
    this.questionCondition = 2;
  }
  else{
    this.questionCondition = 3;
  }
  
  
  }
  submitForm(): void {
    const formValue = new FormData();

    formValue.append('exam', this.question_data.id);
    formValue.append('question', this.mainForm.question);
    formValue.append('media_type', this.mainForm.media_type);
    formValue.append('question_type', this.mainForm.question_type);
    formValue.append('is_multiple', this.mainForm.is_multiple);
    formValue.append('media_name', this.mainForm.media_name);

    this.mainForm.options_list.forEach(element =>{
      formValue.append('option',element);
    })

    if (this.mainForm.media){
      formValue.append('media', this.mainForm.media, this.mainForm.media['name']);
    }
    else{
        formValue.append('media', null);
    }


    this.service.teacher_exam_question_post(formValue).subscribe({
      next: (data) => {
        console.log(data)
        if(data.request_status == 1){
        console.log(data)
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    this.questionList();
  }

  homeWorkSubmitWithQuestion(){
    const data = {
      exam_id: this.question_data.id,
      is_finalized: true,
      method: 'edit'
    };
    this.service.teacher_exam_postFinal(data).subscribe({
      next: (data) => {
        console.log(data)
        if(data.request_status == 1){
        console.log(data)
        alert('Exam is successfully Submitted')
        this.goBack()
      
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  createExam(): void {
    this.router.navigate(['/exams']);
  }

  goBack(): void {
    this.router.navigate(['/exams']);
  }
}