import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddExamQuestionsComponent } from './add-questions.component';

describe('AddQuestionsComponent', () => {
  let component: AddExamQuestionsComponent;
  let fixture: ComponentFixture<AddExamQuestionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddExamQuestionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddExamQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
