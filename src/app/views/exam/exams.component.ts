import { Component, OnInit,TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HomeworksService } from '../../services/homeworks.service';
import { SharedService } from '../../services/shared.service';
@Component({
  selector: 'app-exam',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.scss']
})
export class ExamsComponent implements OnInit {
  @ViewChild('answerModal') answerModal : TemplateRef<any>;
  constructor(
    private service:HomeworksService,
    private fb: FormBuilder,
    private shared:SharedService,
    private router: Router,private modalService: NgbModal,
    public _snackBar: MatSnackBar,
    ) { }

  homeworkForm!: FormGroup;
  submittedForm!: FormGroup;
  areviewForm!: FormGroup;
  allClass = new Array();
  allSection = new Array();
  allSubjects = new Array();
  selectedTab = "CREATE";
  data = null;
  list:any;
  submittedExam:any;
  exam_ans_id:string="";
  
  student_id:string = ""
  filen:string = 'Upload Review';

  ngOnInit(): void {
    this.buildForm();
    this.getAllClass();
    this.teacher_exam_question_post_get();
    this.teacher_submitted_exam_list();
  }

  attach_submit(){
    console.log(this.areviewForm);
    if(this.areviewForm.valid){
      var formValue = new FormData();
      // http://3.17.248.213:8002/teacher_homework_student_list_update_correct_atch/?method=edit&student_id=2&homework_ans_id=68
      // formValue.append('student_id', this.areviewForm.value.student_id);
      // formValue.append('homework_ans_id', this.areviewForm.value.homework_ans_id);
      
      if (this.areviewForm.value.attachment_link){
        formValue.append('attachment_link', this.areviewForm.value.attachment_link, this.areviewForm.value.attachment_link['name']);
      }
      formValue.append('marks_obtain', this.areviewForm.value.marks_obtain);
      const params = {
        student_id: this.student_id,
        
        exam_ans_id: this.exam_ans_id,
        method: 'edit',
        
      }

    this.service.teacher_submitted_exam_attachment_upload(formValue,params).subscribe({
      next: (data) => {
        
        if(data.request_status == 1){
          document.getElementById("modal_close").click();
          this._snackBar.open("Remarks Submitted Successfully",'close', {
            duration: 5000,
         });
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
     }
     else{
       console.log('error');
     }
  }
  selectFile(event){
    if (event.target.files && event.target.files[0]) {
      this.filen = event.target.files[0].name;
      this.areviewForm.patchValue({
        attachment_link: event.target.files[0]
       })
    }
  }
  addQestion(id){
    console.log(id);
      localStorage.setItem('examAdd', JSON.stringify(id));
            this.router.navigate(['/exam/create-exam/add-questions']);
  }
  teacher_submitted_exam_list(): void{
    const params = {
      page_size: '10',
      page: '1',
      session: localStorage.getItem("session_id"),
    }
    // if(search){
    //   params['cls_section'] = this.homeworkForm.value.scl_class;
    //   params['scl_class'] = this.homeworkForm.value.cls_section;
    //   params['subject'] = this.homeworkForm.value.subject;
    // }
    this.service.teacher_exam_question_submitted_get(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
      
          
            this.submittedExam = data.results;
            console.log(this.submittedExam);
            
         
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  teacher_submitted_exam_list_search(){
    const params = {
      page_size: '10',
      page: '1',
      session: '11',
      //session: localStorage.getItem("session_id"),
    }
    
      params['cls_section'] = this.submittedForm.value.cls_section;
      params['scl_class'] = this.submittedForm.value.scl_class;
      params['subject'] = this.submittedForm.value.subject;
    
    this.service.teacher_exam_question_submitted_get(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
      
          
            this.submittedExam = data.results;
            console.log(this.submittedExam);
            
         
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  goToSubmittedExamDetails(status,ans_id,homeid,stu,student_fname,student_lname,subject,stu_class,class_section): void {
    localStorage.setItem("exam_ans_id",ans_id);
    localStorage.setItem("exam_id",homeid);
    localStorage.setItem("student_id",stu);
    localStorage.setItem("student_fname",student_fname);
    localStorage.setItem("student_lname",student_lname);
    localStorage.setItem("subject",subject)
    localStorage.setItem("stu_class",stu_class);
    localStorage.setItem("class_section",class_section);
    if(status == '2'){
      this.router.navigate(['/exam/submitted-exam-remarks-details']);
    } else {
      //this.router.navigate(['/homeworks/submitted-remarks-details']);
      this.router.navigate(['/exam/submitted-exam-details']);
    }
    
  }
  attachmen_modal(homewor_ans_id, student_id){
    
    this.exam_ans_id = homewor_ans_id;
    this.student_id = student_id;
    
    this.modalService.open(this.answerModal,{size:'lg'});
  }
  teacher_exam_question_post_get(index = null, search = false): void {
    const params = {
      page_size: '100',
      page: '1'
    }

    if(index){
      params['id'] = index;
    }

    if(search){
      params['cls_section'] = this.homeworkForm.value.scl_class;
      params['scl_class'] = this.homeworkForm.value.cls_section;
      params['subject'] = this.homeworkForm.value.subject;
    }
    // question-list Exam Question post:
    // http://3.17.248.213:8002//
    // get
    this.service.teacher_exam_question_post_get(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
      
          
            this.list = data.results;
            console.log(this.list);
            
         
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  
  buildForm(): void {
    this.homeworkForm = this.fb.group({
      scl_class: ['',[Validators.required]],
      cls_section: ['',[Validators.required]],
      subject: ['',[Validators.required]]
    });
    
    this.submittedForm = this.fb.group({
      scl_class: ['',[Validators.required]],
      cls_section: ['',[Validators.required]],
      subject: ['',[Validators.required]]
    });
    this.areviewForm= this.fb.group({
      marks_obtain:['',[Validators.required]],
      attachment_link: ['',[Validators.required]],
    })
  }


  getAllClass() {
    this.homeworkForm.controls['cls_section'].setValue('');
    this.homeworkForm.controls['subject'].setValue('');
    this.submittedForm.controls['cls_section'].setValue('');
    this.submittedForm.controls['subject'].setValue('');

    let params = {
      page_size: '100',
      page: '1',
      session: localStorage.getItem("session_id"),
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){

          this.allClass = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  getAllSections() {
    this.homeworkForm.controls['cls_section'].setValue('');
    this.homeworkForm.controls['subject'].setValue('');
    this.submittedForm.controls['cls_section'].setValue('');
    this.submittedForm.controls['subject'].setValue('');

    let params = {
      page_size: '100',
      page: '1',
      session: localStorage.getItem("session_id"),
    }

    params['class'] = this.selectedTab === 'CREATE' ? this.homeworkForm.value.scl_class : this.submittedForm.value.scl_class;

    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSection = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  getAllSubjects() {
    const params = {
      page_size: '100',
      page: '1',
    }

    params['scl_class_id'] = this.selectedTab === 'CREATE' ? this.homeworkForm.value.scl_class : this.submittedForm.value.scl_class;

    return this.shared.school_subject_class_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSubjects = data['results'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  classChange(e) { 
    this.getAllSections();
    this.getAllSubjects();
  }

  changeSelectedTab(tab): void {
    
    if(tab == 'HISTORY'){
      this.selectedTab = tab;
    } else {
      this.router.navigate(['/exam/create-exam']);
    }
  }

  changeStatusQuestion(id,index, status): void{
    // this.service.teacher_homework_active_inactive(id,{"is_active": status }).subscribe({
    //   next: (data) => {
    //     if(data.request_status == 1){
    //       this.data[index].is_active = status;
    //     }
    //   },
    //   error: (error) => {
    //     console.error(error)
    //   },
    //   complete: () => {
    //     // console.info('complete')
    //   }
    // });
  }

  goToSubmittedHomeworkDetails(): void {
    this.router.navigate(['/exam/submitted-exam-details']);
  }

  goBack(): void {
    this.router.navigate(['/']);
  }
  
  applyCss(form:FormGroup, field:string)
  {
    return this.shared.displayFieldCss(form,field);
  }
  validField(form:FormGroup,field:string)
  {
    return this.shared.isFieldValid(form,field);
  }
}
