import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SharedService } from '../../services/shared.service';
import { NgForm } from '@angular/forms';
import { NgbModal, NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AttendanceService } from 'src/app/services/attendance.service';
@Component({
  selector: 'app-student-attendance',
  templateUrl: './student-attendance.component.html',
  styleUrls: ['./student-attendance.component.scss']
})
export class StudentAttendanceComponent implements OnInit {
  pageSize: string = "0";
  pageNo: string = "1";
  startdate:string = '';
  enddate:string = '';
  studentAttendanceData:any;
  @ViewChild('docModal') docModal : TemplateRef<any>; // Note:
   
  @ViewChild('allModal') allModal : TemplateRef<any>; // Note:

  constructor(private _snackBar: MatSnackBar, private router: Router, private modalService: NgbModal , private fb: FormBuilder, private shared: SharedService, private attendanceService:AttendanceService) { }
 StartDate:string='';
 EndDate:string='';
  updateForm: FormGroup;
  viewForm: FormGroup;
  allClass :any;

  table:boolean=false;
  indexs;
  allSection = new Array();
  allSubjects = new Array();
  selectedTab = "CREATE";
  session = localStorage.getItem('session_id');
  allsubject: any;
  attendence_type: any;
  attendence_list: any;
  atn_list:any;
  atten_type = [];
  teachers_comment = [];
  student_list: any;
  cls_section;
  scl_class;
  durationInSeconds = 5;
  class:string = '';
  section:string = '';
  attendanceList: any;

  
  ngOnInit(): void {
    this.buildForm();
    this.getAllClass();
    this.attendence_type = localStorage.getItem('school_attendence_type');
    console.log(this.attendence_type);
    this.student_list = [];
  }


  buildForm(): void {
    this.updateForm = this.fb.group({
      scl_class: ['', [Validators.required]],
      cls_section: ['', [Validators.required]]
    });
    if (this.attendence_type == 'once') {
      this.updateForm.addControl('routine', new FormControl(''));
    } else {
      this.updateForm.addControl('routine', new FormControl('', Validators.required));
    }
    this.viewForm = this.fb.group({
      scl_class: ['', [Validators.required]],
      cls_section: ['', [Validators.required]],
      routine: ['', [Validators.required]]
    });
  }

  getAllClass() {
    // this.updateForm.controls['cls_section'].setValue('SELECT');
    // this.viewForm.controls['cls_section'].setValue('SELECT');
    // this.viewForm.controls['subject'].setValue('SELECT');

    let params = {
      page_size: '100',
      page: '1',
      session: this.session,
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        console.log(data);
        if (data.request_status == 1) {
          this.allClass = data['result'];
          
        }
      },
      error: (error) => {
        this._snackBar.open(error,'close', {
          duration: 5000,
       });
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

 
  getDetails(student:any){
    this.router.navigate(['/attendancedetails']);
    localStorage.setItem('StudentId',student);
  }

  getAllSections() {
    // this.updateForm.controls['cls_section'].setValue('SELECT');
    // this.viewForm.controls['cls_section'].setValue('SELECT');
    // this.viewForm.controls['subject'].setValue('SELECT');

    let params = {
      page_size: '100',
      page: '1',
      session: localStorage.getItem('session_id'),
    }

    params['class'] = this.selectedTab === 'CREATE' ? this.updateForm.value.scl_class : this.viewForm.value.scl_class;

    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if (data.request_status == 1) {
          this.allSection = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });

  };

  getAllSubjects() {
    const params = {
      page_size: '100',
      page: '1',
    }

    params['scl_class_id'] = this.selectedTab === 'CREATE' ? this.updateForm.value.scl_class : this.viewForm.value.scl_class;

    return this.shared.school_subject_class_crud(params).subscribe({
      next: (data) => {
        if (data.request_status == 1) {
          this.allSubjects = data['results'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });

  };

  classChange(e) {
    this.scl_class = this.updateForm.value.scl_class;
    this.getAllSections();
    this.getAllSubjects();
    this.sectionChange();
  }

  // startDate(e){
  //   this.startDate = this.updateForm.value.startdate;
  // }
  // endDate(e){
  //   console.log(e);
  //   this.endDate = this.updateForm.value.enddate;
  // }
  myFunction(){
    this.StartDate=(<HTMLInputElement>document.getElementById('sdate')).value;
    localStorage.setItem('strDate',this.StartDate);

  }
  myFunction1(){
    this.EndDate=(<HTMLInputElement>document.getElementById('edate')).value;
    localStorage.setItem('enDate',this.EndDate);

  }


  changeSelectedTab(tab): void {
    this.selectedTab = tab;
  }

  goBack(): void {
    this.router.navigate(['/']);
  }
  remarks_add(){
    var rem = (<HTMLInputElement>document.getElementById('tremarks')).value;

    this.teachers_comment.splice(this.indexs, 1, rem);
    
    document.getElementById("modal_close").click();
    this._snackBar.open("Remarks Added Successfully",'close', {
      duration: 5000,
   });
  }
  sectionChange() {
    this.cls_section = this.updateForm.value.cls_section;
    if (this.attendence_type != "once") {

      const params = {
        scl_class: this.updateForm.value.scl_class,
        cls_section: this.updateForm.value.cls_section,
      }
      this.shared.teacher_routine_list_by_daywise(params).subscribe({
        next: (data) => {
          this.allsubject = data.result;
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
      console.log(params);
    }
  }

  OnSearching(){
    let params = {
      page_size: '0',
      scl_class:this.viewForm.value.scl_class,
      cls_section:this.viewForm.value.cls_section,
      start_date:this.StartDate,
      end_date:this.EndDate,
      session: this.session,
    }
    this.shared.attendence_student_list_get(params).subscribe({
      next: (data) => {
        console.log(data);
        this.studentAttendanceData=data.results;
        this.table=true;
      },
      complete: () => {
        // console.info('complete')
      }
    });
  
  }
  onSubmit(f: NgForm) {
    //console.log(f.present_status_1.value);  // { first: '', last: '' }
    if (f.valid == false) {
      
      this._snackBar.open("Please Fillup all fields",'close', {
        duration: 5000,
     });
    } else {
       this.attendence_list= [];
      for (var i = 0; i < this.student_list.length; i++) {
        
        if (this.teachers_comment[i] === undefined) {
          var o = {
            student_id: this.student_list[i].student,
            teacher_remarks:'',
            routine: this.updateForm.value.routine,
            present_status: this.atten_type[i],
            attendence_type: this.attendence_type,
            cls_section: this.cls_section,
            scl_class: this.scl_class,
            date:new Date().toISOString().slice(0, 10),
  
          };
          this.attendence_list.push(o);
        } else {
        var j = {
          student_id: this.student_list[i].student,
          teacher_remarks:this.teachers_comment[i],
          routine: this.updateForm.value.routine,
          present_status: this.atten_type[i],
          attendence_type: this.attendence_type,
          cls_section: this.cls_section,
          scl_class: this.scl_class,
          date:new Date().toISOString().slice(0, 10),

        };
        this.attendence_list.push(j);
      }
       
        
      }
      console.log(this.attendence_list);
      let body ={
        attendence_list:this.attendence_list,
      }
      this.shared.student_attendence(body).subscribe({
        next: (data) => {
          this._snackBar.open("Attendence Submitted Successfully",'close', {
            duration: 5000,
         });
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
    }
    console.log(f.valid);  // false
  }
  getstudent() {
    if (this.attendence_type == 'once') {

    } else {
      if (this.updateForm.valid) {
        const params = {
          session: this.session,
          class: this.updateForm.value.scl_class,
          section: this.updateForm.value.cls_section,
          attendence_type: this.attendence_type,
          routine: this.updateForm.value.routine
        };
        this.shared.attendence_check(params).subscribe(
          res => {
            console.log(res);
            if (res.msg == "Not Submitted") {
              const params = {
                session: this.session,
                class: this.updateForm.value.scl_class,
                section: this.updateForm.value.cls_section,
                page_size: 0
              };
              this.shared.attendence_student_list_get(params).subscribe(
                res => {
                  console.log(res);
                  this.student_list = res.results;
                  for (var i = 0; i < this.student_list.length; i++) {
                    this.teachers_comment[i] = '';
                  }
                })
            } else if(res.msg =="Student Not Found"){
              this._snackBar.open("Student Not Found",'close', {
                duration: 5000,
             });
             this.student_list =[];
            }
            else {
              
              this._snackBar.open("Already Submitted",'close', {
                duration: 5000,
             });
            }
          })
      } else {
        this._snackBar.open("Select required fields",'close', {
          duration: 5000,
       });
        
      }
    }

  }
  all_remarks_add(){
    var pstatus = (<HTMLInputElement>document.querySelector('input[name="all_present_status"]:checked')).value;
    var tmark = (<HTMLInputElement>document.getElementById('alltremarks')).value;
    this.attendence_list= [];
      for (var i = 0; i < this.student_list.length; i++) {

          var o = {
            student_id: this.student_list[i].student,
            teacher_remarks:tmark,
            routine: this.updateForm.value.routine,
            present_status: pstatus,
            attendence_type: this.attendence_type,
            cls_section: this.cls_section,
            scl_class: this.scl_class,
            date:new Date().toISOString().slice(0, 10),
  
          };
          this.attendence_list.push(o);

       
        
      }
      console.log(this.attendence_list);
      let body ={
        attendence_list:this.attendence_list,
      }
      this.shared.student_attendence(body).subscribe({
        next: (data) => {
          this._snackBar.open("Student Attendence submitted",'close', {
            duration: 5000,
         });
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
  }
  
  teacher_remarks(s){
    this.indexs = s;
    this.modalService.open(this.docModal,{ size: 'lg', backdrop: 'static' });
  }
  all_modal(){
    this.modalService.open(this.allModal,{ size: 'lg', backdrop: 'static' });
  }
  
  applyCss(form:FormGroup, field:string)
  {
    return this.shared.displayFieldCss(form,field);
  }
  validField(form:FormGroup,field:string)
  {
    return this.shared.isFieldValid(form,field);
  }
  getAttendanceDetails() {
    var params = {
      page_size: this.pageSize,
      session: this.session,
      scl_class: this.class,
      cls_section: this.section,
      start_date: this.startdate,
      end_date: this.enddate
    }
    
    this.attendanceService.getAttendanceDetails(params).subscribe((responce) => {
      console.log(responce);
     this.attendanceList = responce.results;

    });
  }
}
