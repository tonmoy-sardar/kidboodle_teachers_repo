import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { HomeworksService } from '../../../services/homeworks.service';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal, NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-submitted-details',
  templateUrl: './submitted-details.component.html',
  styleUrls: ['./submitted-details.component.scss']
})
export class SubmittedDetailsComponent implements OnInit {
homework_ans_id:string='';
homework_id:string="";
student_id:string="";
questionarray:any="";
student_fname:string="";
student_lname:string="";
subject:string="";
answer_type = [];
indexs;
teachers_comment = [];
stu_class:string="";
class_section:string="";
all_ans_list:any;
@ViewChild('remarksModal') remarksModal : TemplateRef<any>; // Note:
  constructor( private modalService: NgbModal ,private _snackBar: MatSnackBar,private service: HomeworksService,private router: Router) { }

  ngOnInit(): void {
    this.homework_ans_id = localStorage.getItem('homework_ans_id');
    this.homework_id = localStorage.getItem('homework_id');
    this.student_id = localStorage.getItem('student_id');
    this.student_fname = localStorage.getItem("student_fname");
    this.student_lname = localStorage.getItem("student_lname");
    this.subject = localStorage.getItem("subject")
    this.stu_class = localStorage.getItem("stu_class");
    this.class_section = localStorage.getItem("class_section");

    this.getquestion();
  }

  goBack(): void {
    this.router.navigate(['/homeworks']);
  }
  onSubmit(f: NgForm){
    if (f.valid == false) {
      
        this._snackBar.open("Please Fillup all fields",'close', {
          duration: 5000,
      });
    } else {
      this.all_ans_list= [];
      for (var i = 0; i < this.questionarray.length; i++) {

          var o = {
            hw_ans_det_id: this.questionarray[i].id,
            comment:this.teachers_comment[i],
            is_right:this.answer_type[i]
          };
          this.all_ans_list.push(o);  
      }
      console.log(this.answer_type);
      let body ={
        all_ans_list:this.all_ans_list,
      }
      const params = {
        homework_id: this.homework_id,
        method: 'edit',
      }
      this.service.teacher_online_homework_remarks_submit(body,params).subscribe({
        next: (data) => {
          this._snackBar.open("Remarks Submitted Successfully",'close', {
            duration: 5000,
         });
         localStorage.setItem("remarks_submitted",'success');
         this.router.navigate(['/homeworks']);
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
    }
  }
  teacher_remarks(s){
    this.indexs = s;
    this.modalService.open(this.remarksModal,{ size: 'lg', backdrop: 'static' });
  }
  remarks_add(){
    var rem = (<HTMLInputElement>document.getElementById('tremarks')).value;
    (<HTMLInputElement>document.getElementById('tremarks')).value = '';
    this.teachers_comment.splice(this.indexs, 1, rem);
    
    document.getElementById("modal_close").click();
    this._snackBar.open("Remarks Added Successfully",'close', {
      duration: 5000,
   });
  }
  getquestion()
  {
    const params = {
      student_id: this.student_id,
      
      homework_ans_id: this.homework_ans_id,
      homework_id: this.homework_id,
    }

  this.service.teacher_submitted_homework_details_view(params).subscribe({
    next: (data) => {
      console.log(data);
      this.questionarray = data.results;
      for (var i = 0; i < this.questionarray.length; i++) {
        this.teachers_comment[i] = '';
      }
    },
    error: (error) => {
      console.error(error)
    },
    complete: () => {
      // console.info('complete')
    }
  });
  }

}
