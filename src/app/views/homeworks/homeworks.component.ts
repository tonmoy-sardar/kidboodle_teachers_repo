import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HomeworksService } from '../../services/homeworks.service';
import { SharedService } from '../../services/shared.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-homeworks',
  templateUrl: './homeworks.component.html',
  styleUrls: ['./homeworks.component.scss']
})
export class HomeworksComponent implements OnInit {
  @ViewChild('editModal') editModal : TemplateRef<any>;
  @ViewChild('saveModal') saveModal : TemplateRef<any>;
  @ViewChild('imageModal') imageModal : TemplateRef<any>;
  @ViewChild('answerModal') answerModal : TemplateRef<any>;


  constructor(
    private service:HomeworksService,
    private modalService: NgbModal, 
    private fb: FormBuilder,
    private shared:SharedService,
    private _snackBar: MatSnackBar, 
    private router: Router) { }

  homeworkForm!: FormGroup;
  submittedForm!: FormGroup;
  areviewForm!: FormGroup;
  allClass = new Array();
  allSection = new Array();
  allSubjects = new Array();
  allClasssub = new Array();
  allSubjectssub = new Array();
  allSectionsub = new Array();
  selectedTab = "CREATE";
  data = null;
  image1:string="";
  modalValue:any;
  modalData:any;
  submittedhomework:any;
  totalItems;
  page: number = 1;
  pageSize:number = 10;
  homewor_ans_id:string=""
  student_id:string = ""
  filen:string = 'Upload Review';
  
  ispdfSrc = false;
  ngOnInit(): void {
    this.buildForm();
    this.getAllClass();
    this.getAllClasssub();
    this.teacher_homework_view_list();
    this.teacher_submitted_homework_list();
    if(localStorage.getItem('remarks_submitted') == 'success'){
      this._snackBar.open("Remarks Submitted Successfully",'close', {
        duration: 5000,
     });
    }
    
   localStorage.setItem("remarks_submitted",'');
  }
  teacher_submitted_homework_list(){
    const params = {
      page_size: '10',
      page: '1',
      session: localStorage.getItem("session_id"),
      //session: '11',
    }
    this.service.teacher_submitted_homework_view_list(params).subscribe({
      next: (data) => {
        console.log(data);
       this.submittedhomework = data.results
        // this.image1=data.results.attachment_link;
        // if(data.request_status == 1){
        //   if(index){
        //     localStorage.setItem('current_question_data', JSON.stringify(data.results[0]));
        //     this.router.navigate(['/homeworks/create-homework/add-questions']);
        //   }else{
        //     this.data = data.results;
        //     this.totalItems = data.count;          
        //   }
        // }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  teacher_homework_view_list(index = null, search = false): void {
    const params = {
      page_size: '100',
      page: '1'
    }

    if(index){
      params['id'] = index;
    }

    if(search){
      params['cls_section'] = this.homeworkForm.value.scl_class;
      params['scl_class'] = this.homeworkForm.value.cls_section;
      params['subject'] = this.homeworkForm.value.subject;
      
    }

    this.service.teacher_homework_view_list(params).subscribe({
      next: (data) => {
        console.log(data);
       
        this.image1=data.results.attachment_link;
        if(data.request_status == 1){
          if(index){
            localStorage.setItem('current_question_data', JSON.stringify(data.results[0]));
            this.router.navigate(['/homeworks/create-homework/add-questions']);
          }else{
            this.data = data.results;
            this.totalItems = data.count;          
          }
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  attachmen_modal(homewor_ans_id, student_id){
    
    this.homewor_ans_id = homewor_ans_id;
    this.student_id = student_id;
    
    this.modalService.open(this.answerModal,{size:'lg'});
  }
  attach_submit(){
    console.log(this.areviewForm);
    if(this.areviewForm.valid){
      var formValue = new FormData();
      // http://3.17.248.213:8002/teacher_homework_student_list_update_correct_atch/?method=edit&student_id=2&homework_ans_id=68
      // formValue.append('student_id', this.areviewForm.value.student_id);
      // formValue.append('homework_ans_id', this.areviewForm.value.homework_ans_id);
      
      if (this.areviewForm.value.attachment_link){
        formValue.append('attachment_link', this.areviewForm.value.attachment_link, this.areviewForm.value.attachment_link['name']);
      }
      const params = {
        student_id: this.student_id,
        
        homework_ans_id: this.homewor_ans_id,
        method: 'edit',
      }

    this.service.teacher_submitted_homework_attachment_upload(formValue,params).subscribe({
      next: (data) => {
        console.log(data);
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
     }
     else{
       console.log('error');
     }
  }
  selectFile(event){
    if (event.target.files && event.target.files[0]) {
      this.filen = event.target.files[0].name;
      this.areviewForm.patchValue({
        attachment_link: event.target.files[0]
       })
    }
  }
  teacher_submitted_homework_view_list(){
    const params = {
      page_size: '10',
      page: '1'
    }
    
      params['cls_section'] = this.submittedForm.value.cls_section;
      params['scl_class'] = this.submittedForm.value.scl_class;
      params['subject'] = this.submittedForm.value.subject;
      params['session'] = localStorage.getItem("session_id");
      this.service.teacher_submitted_homework_view_list(params).subscribe({
        next: (data) => {
          console.log(data);
          this.submittedhomework = data.results;
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
    
  }

  
  buildForm(): void {
    this.homeworkForm = this.fb.group({
      scl_class: ['SELECT',[Validators.required]],
      cls_section: ['SELECT',[Validators.required]],
      subject: ['SELECT',[Validators.required]]
    });
    
    this.submittedForm = this.fb.group({
      scl_class: ['',[Validators.required]],
      cls_section: ['',[Validators.required]],
      subject: ['',[Validators.required]]
    });

    this.areviewForm= this.fb.group({
      
      attachment_link: ['',[Validators.required]],
    })

  }


  getAllClass() {
    this.homeworkForm.controls['cls_section'].setValue('SELECT');
    let params = {
      // page_size: '100',
      // page: '1',
       session: localStorage.getItem("session_id"),
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allClass = data['result'];
          console.log(this.allClass);
          
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  getAllClasssub(){
    this.submittedForm.controls['cls_section'].setValue('');
    let params = {
      // page_size: '100',
      // page: '1',
       session: localStorage.getItem("session_id"),
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allClasssub = data['result'];
          
          
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  getAllSections() {
    this.homeworkForm.controls['cls_section'].setValue('SELECT');
    let params = {
      // page_size: '100',
      // page: '1',
       session: localStorage.getItem("session_id"),
      class: this.homeworkForm.value.scl_class
    }
    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSection = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  getAllSubjects() {
    this.homeworkForm.controls['subject'].setValue('SELECT');
    let params = {
      // page_size: '100',
      // page: '1',
      scl_class: this.homeworkForm.value.scl_class,
    }
    return this.shared.school_subject_class_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSubjects = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };
  /*subn=mitted Homework*/
  getAllSections2() {
    this.submittedForm.controls['cls_section'].setValue('');
    let params = {
      // page_size: '100',
      // page: '1',
       session: localStorage.getItem("session_id"),
      class: this.submittedForm.value.scl_class
    }
    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSectionsub = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  getAllSubjects2() {
    this.submittedForm.controls['subject'].setValue('');
    let params = {
      // page_size: '100',
      // page: '1',
      scl_class: this.submittedForm.value.scl_class,
    }
    return this.shared.school_subject_class_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSubjectssub = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  classChange(e) { 
    console.log(e.target.value);
    this.getAllSections();
    this.getAllSubjects();
  }
  classChange2(e){
    this.getAllSections2();
    this.getAllSubjects2();
  }

  openModel(id:number){
    this.modalService.open(this.editModal,{size:'lg'});
    var params={
      page_size: 1,
      id:id
    }

    this.service.teacher_homework_view_list(params).subscribe({
      next: (data) => {
        console.log(data);
        this.modalValue=data.results;
      }
    })
  }

  editModal_Image_Click(image){
    console.log(image);
    let imageExt = image.split('.')[4].toLowerCase();
    if(imageExt ==='jpg' || imageExt ==='jpeg' || imageExt ==='png' || imageExt ==='jfif' ||  imageExt ==='gif')
    {
      this.modalService.open(this.imageModal,{size:'lg'})
    }
    else if( imageExt ==='pdf'){
      this.ispdfSrc = true;
      this.modalService.open(this.imageModal,{size:'lg'});
    }
  }

  openModel2(id:number){
    this.modalService.open(this.saveModal,{size:'lg'});
    const params = {
      page_size: '100',
      page: '1',
      homework_id:id
    }
    this.service.teacher_homework_question_details(params).subscribe({
      next: (data) => {
        console.log(data)
        if(data.request_status == 1){
        console.log(data,'questionList')
        this.modalData = data.results
        console.log(this.modalData);
        
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });

  }

  changeSelectedTab(tab): void {
    if(tab == 'HISTORY'){
      this.selectedTab = tab;
    } else {
      this.router.navigate(['/homeworks/create-homework']);
    }
  }

  changeStatusQuestion(id,index, status): void{
    this.service.teacher_homework_active_inactive(id,{"is_active": status }).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.data[index].is_active = status;
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }
  
  handlePageChange(event) {
    this.page = event;
  }
  goToSubmittedHomeworkDetails(status,ans_id,homeid,stu,student_fname,student_lname,subject,stu_class,class_section): void {
    localStorage.setItem("homework_ans_id",ans_id);
    localStorage.setItem("homework_id",homeid);
    localStorage.setItem("student_id",stu);
    localStorage.setItem("student_fname",student_fname);
    localStorage.setItem("student_lname",student_lname);
    localStorage.setItem("subject",subject)
    localStorage.setItem("stu_class",stu_class);
    localStorage.setItem("class_section",class_section);
    if(status == '2'){
      this.router.navigate(['/homeworks/submitted-remarks-details']);
    } else {
      //this.router.navigate(['/homeworks/submitted-remarks-details']);
      this.router.navigate(['/homeworks/submitted-details']);
    }
    
  }

  goBack(): void {
    this.router.navigate(['/']);
  }
  
}
