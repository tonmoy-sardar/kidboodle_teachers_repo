import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { HomeworksService } from '../../../services/homeworks.service';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal, NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-submitted-review-details',
  templateUrl: './submitted-review-details.component.html',
  styleUrls: ['./submitted-review-details.component.scss']
})
export class SubmittedReviewDetailsComponent implements OnInit {
homework_ans_id:string='';
homework_id:string="";
student_id:string="";
questionarray:any="";
student_fname:string="";
student_lname:string="";
subject:string="";
answer_type = [];
indexs;
teachers_comment = [];
stu_class:string="";
class_section:string="";
all_ans_list:any;
comments:string;
@ViewChild('remarksModal') remarksModal : TemplateRef<any>; // Note:
  constructor( private modalService: NgbModal ,private _snackBar: MatSnackBar,private service: HomeworksService,private router: Router) { }

  ngOnInit(): void {
    this.homework_ans_id = localStorage.getItem('homework_ans_id');
    this.homework_id = localStorage.getItem('homework_id');
    this.student_id = localStorage.getItem('student_id');
    this.student_fname = localStorage.getItem("student_fname");
    this.student_lname = localStorage.getItem("student_lname");
    this.subject = localStorage.getItem("subject")
    this.stu_class = localStorage.getItem("stu_class");
    this.class_section = localStorage.getItem("class_section");

    this.getquestion();
  }

  goBack(): void {
    this.router.navigate(['/homeworks']);
  }
  
  teacher_remarks(s){
    if(s == ''){
      this.comments = 'No Remarks';
    }else {
      this.comments = s;
    }
    

    this.modalService.open(this.remarksModal,{ size: 'lg', backdrop: 'static' });
  }
  
  getquestion()
  {
    const params = {
      student_id: this.student_id,
      
      homework_ans_id: this.homework_ans_id,
      homework_id: this.homework_id,
    }

  this.service.teacher_submitted_homework_remarks_view(params).subscribe({
    next: (data) => {
      console.log(data);
      this.questionarray = data.results;
      
    },
    error: (error) => {
      console.error(error)
    },
    complete: () => {
      // console.info('complete')
    }
  });
  }

}
