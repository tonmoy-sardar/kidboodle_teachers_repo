import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittedReviewDetailsComponent } from './submitted-review-details.component';

describe('SubmittedReviewDetailsComponent', () => {
  let component: SubmittedReviewDetailsComponent;
  let fixture: ComponentFixture<SubmittedReviewDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubmittedReviewDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedReviewDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
