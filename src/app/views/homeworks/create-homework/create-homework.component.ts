import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SharedService } from '../../../services/shared.service';
import { HomeworksService } from '../../../services/homeworks.service';

@Component({
  selector: 'app-create-homework',
  templateUrl: './create-homework.component.html',
  styleUrls: ['./create-homework.component.scss']
})
export class CreateHomeworkComponent implements OnInit {
public fileDiv = false;
public onlineDiv = false;
public ContentTypeProfileBanner: any;
isSuccess:boolean = false;
  snackBar: any;
  constructor(private service:HomeworksService, private fb: FormBuilder,private router: Router,private shared:SharedService) { }

  homeworkForm!: FormGroup;
  allClass = new Array();
  allSection = new Array();
  allSubjects = new Array();

  ngOnInit(): void {
    this.buildForm();
    this.getAllClass();
  }

  
  buildForm(): void {
    this.homeworkForm = this.fb.group({
      title: [null,[Validators.required]],
      description: [null,[Validators.required]],
      start_date: [null,[Validators.required]],
      end_date: [null,[Validators.required]],
      question_type: ['SELECT',[Validators.required]],
      scl_class: ['SELECT',[Validators.required]],
      cls_section: ['SELECT',[Validators.required]],
      subject: ['SELECT',[Validators.required]],
      attachment_link: [null,[Validators.required]],
      attachement: [''],
      status:['active'],
      
    });
  }

  getAllClass() {
    this.homeworkForm.controls['cls_section'].setValue('SELECT');
    let params = {
      // page_size: '100',
      // page: '1',
      // session: 3,
    }
    this.shared.getClassDetailsdata(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allClass = data['result'];
          console.log(this.allClass);
          
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
  }

  getAllSections() {
    this.homeworkForm.controls['cls_section'].setValue('SELECT');
    let params = {
      // page_size: '100',
      // page: '1',
      // session: 3,
      class: this.homeworkForm.value.scl_class
    }
    return this.shared.school_class_section_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSection = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  getAllSubjects() {
    this.homeworkForm.controls['subject'].setValue('SELECT');
    let params = {
      // page_size: '100',
      // page: '1',
      scl_class: this.homeworkForm.value.scl_class,
    }
    return this.shared.school_subject_class_crud(params).subscribe({
      next: (data) => {
        if(data.request_status == 1){
          this.allSubjects = data['result'];
        }
      },
      error: (error) => {
        console.error(error)
      },
      complete: () => {
        // console.info('complete')
      }
    });
    
  };

  classChange(e) { 
    console.log(e.target.value);
	
    this.getAllSections();
    this.getAllSubjects();
  }



  routeToAddQuestionsPage(){
    this.router.navigate(['/homeworks/create-homework/add-questions']);
  }

  goBack(): void {
    this.router.navigate(['/homeworks']);
  }

fileAttached(e){
 console.log(e.target.value);
 if(e.target.value == 'attachment')
 {
   this.fileDiv = true;
 }
 else
 {
   this.fileDiv = false;
 }
 
}

  addQuestionImage(event: any) {
    console.log(event);
    
    const reader = new FileReader();
    if (event.target.files.length) {
      this.ContentTypeProfileBanner = event.target.files[0];
      console.log(event.target.files[0]);
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      // this.fileType = this.ProfilePic.name.split('.').pop();
      // if (this.f.media_type.value === '1') {
      if (
        this.ContentTypeProfileBanner.type === 'image/jpg' ||
        this.ContentTypeProfileBanner.type === 'image/jpeg' ||
        this.ContentTypeProfileBanner.type === 'image/png' ||
        this.ContentTypeProfileBanner.type === 'image/gif'
      ) {
        // this.formControlsGroup1.profilePic.setValue(this.ContentTypeProfileBanner)
        console.log(this.ContentTypeProfileBanner);


      } else {
        this.ContentTypeProfileBanner = null;
        // this.f.advertisement.setValue(null);
        this.ContentTypeProfileBanner = '';
        // this.previewUrl = '';
        this.isSuccess = false;
        this.shared.openSnackBar('Only Image file is accepted', 'X',this.isSuccess);
      }
    }
  }

  submitForm(): void {
    const start_date = this.shared.formatDate(this.homeworkForm.value.start_date);
      this.homeworkForm.patchValue({
        start_date: start_date
      })
      const end_date = this.shared.formatDate(this.homeworkForm.value.end_date);
      this.homeworkForm.patchValue({
        end_date: end_date
      })
      console.log(this.homeworkForm.value);
      
      let formData = new FormData();
        formData.append('title', this.homeworkForm.value.title);
        formData.append('description', this.homeworkForm.value.description);
        formData.append('start_date', this.homeworkForm.value.start_date);
        formData.append('end_date', this.homeworkForm.value.end_date);
        formData.append('question_type', this.homeworkForm.value.question_type);
        formData.append('scl_class', this.homeworkForm.value.scl_class);
        formData.append('cls_section', this.homeworkForm.value.cls_section);
        formData.append('subject', this.homeworkForm.value.subject);
        if (this.ContentTypeProfileBanner) {
          formData.append('attachement',this.ContentTypeProfileBanner);
        } else {
          formData.append('attachement','');
        }
        
        formData.append('status', this.homeworkForm.value.status);

      this.service.teacher_homework_post(formData).subscribe({
        next: (data) => {
          if(data.request_status == 1){
            localStorage.setItem('current_question_data', JSON.stringify(data.results));
            // this.routeToAddQuestionsPage();
            if (data.results?.question_type === 'online') {
              this.routeToAddQuestionsPage();
            } else {
              this.router.navigate(['/homeworks']);
            }
          }
        
          
        },
        error: (error) => {
          console.error(error)
        },
        complete: () => {
          // console.info('complete')
        }
      });
  }

}
