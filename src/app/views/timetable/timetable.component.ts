import { Component, OnInit } from '@angular/core';
import { getTime } from 'date-fns';
import { Router } from '@angular/router';
import { TimetableService } from 'src/app/services/timetable.service';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.scss']
})
export class TimetableComponent implements OnInit {

  timeslot:any;
  periods:any;
  dayperiod:any[];
  resultArray:any;
  monday:any;
  tuesday:any;
  wednesday:any;
  thursday:any;
  friday:any;
  saturday:any;
  sunday:any;
  mon:any;
  tue:any;
  wed:any;
  thu:any;
  fri:any;
  sat:any;
  sun:any;
  constructor(private _timetableService:TimetableService, private router:Router) { }

  ngOnInit(): void {
    this.getData(); 
  }
  getData()
  {
    const paramRoutineWeb = {
      page_size:'0'
    };
    this._timetableService.gettimeSlot(paramRoutineWeb).subscribe(response =>{
      var time = response['results'];
      time.sort(function (a, b) {
        return a.start_time.localeCompare(b.start_time);
    });
    
      
     this.timeslot = time;
     
     this._timetableService.getteacherRoutine(paramRoutineWeb).subscribe( res =>{
      this.mon = res.results.mon;
      this.tue = res.results.tue;
      this.wed = res.results.wed;
      this.thu = res.results.thu;
      this.fri = res.results.fri;
      this.sat = res.results.sat;
      this.sun = res.results.sun;
      console.log(this.tue);

      var tues = [];
      var mond = [];
      var wedn = [];
      var thur = [];
      var frid = [];
      var satu = [];
      var sund = [];
      for (let i = 0; i < time.length; i++) {
          var present = 0;
          var tpresent = 0;
          var wpresent = 0;
          var thpresent = 0;
          var fpresent = 0;
          var spresent = 0;
          var supresent = 0;
  
            for (var item of this.mon) {
              if(item.time_slot == time[i].id){
                var o = {
                  class_name  : item.class_name,
                  is_live   : item.is_live,
                  section : item.section,
                  subject : item.subject,
                  zoom_link : item.zoom_link,
                  zoom_pass : item.zoom_pass,
              }
          
               this.monday.push(o);
               present = 1;
               
            }
            this.monday = mond;
       }
       for (var item1 of this.tue) {
        this.tuesday = tues;
        if(item1.time_slot == time[i].id){
          
          var t = {
            class_name  : item1.class_name,
            is_live   : item1.is_live,
            section : item1.section,
            subject : item1.subject,
            zoom_link : item1.zoom_link,
            zoom_pass : item1.zoom_pass,
        }
    
         this.tuesday.push(t);
         tpresent = 1;
         
      }
      
    }
      for (var item2 of this.wed) {
        this.wednesday = wedn;
        if(item2.time_slot == time[i].id){
          var b = {
            class_name  : item2.class_name,
            is_live   : item2.is_live,
            section : item2.section,
            subject : item2.subject,
            zoom_link : item2.zoom_link,
            zoom_pass : item2.zoom_pass,
        }
    
         this.wednesday.push(b);
         wpresent = 1;
         
      }
    }
    for (var item3 of this.thu) {
      if(item3.time_slot == time[i].id){
        var c = {
          class_name  : item3.class_name,
          is_live   : item3.is_live,
          section : item3.section,
          subject : item3.subject,
          zoom_link : item3.zoom_link,
          zoom_pass : item3.zoom_pass,
      }
  
       this.thursday.push(c);
       thpresent = 1;
       
    }
  }
  for (var item4 of this.fri) {
    if(item4.time_slot == time[i].id){
      var d = {
        class_name  : item4.class_name,
        is_live   : item4.is_live,
        section : item4.section,
        subject : item4.subject,
        zoom_link : item4.zoom_link,
        zoom_pass : item4.zoom_pass,
    }

     this.friday.push(d);
     fpresent = 1;
     
  }
}
for (var item5 of this.sat) {
  if(item5.time_slot == time[i].id){
    var e = {
      class_name  : item5.class_name,
      is_live   : item5.is_live,
      section : item5.section,
      subject : item5.subject,
      zoom_link : item5.zoom_link,
      zoom_pass : item5.zoom_pass,
  }

   this.saturday.push(e);
   spresent = 1;
   
}
}
          for (var item6 of this.sun) {
            if(item6.time_slot == time[i].id){
              var f = {
                class_name  : item6.class_name,
                is_live   : item6.is_live,
                section : item6.section,
                subject : item6.subject,
                zoom_link : item6.zoom_link,
                zoom_pass : item6.zoom_pass,
            }

            this.sunday.push(d);
            supresent = 1;
            
          }
          }
             
              this.tuesday = tues;
              this.wednesday = wedn;
              this.thursday = thur;
              this.friday = frid;
              this.saturday = satu;
              this.sunday = sund;
 
 var z = {
  class_name  : '',
  is_live   : '',
  section : '',
  subject : '',
  zoom_link : '',
  zoom_pass : '',
}
       if(present == 0){
       this.monday.push(z);         
      }
      if(tpresent == 0){this.tuesday.push(z);}
      if(wpresent == 0){this.wednesday.push(z);}
      if(thpresent == 0){this.thursday.push(z);}
      if(fpresent == 0){this.friday.push(z);}
      if(spresent == 0){this.saturday.push(z);}
      if(supresent == 0){this.sunday.push(z);}  
    
      }
      
    
    });

   
      
    
  });
}
goBack(): void {
  this.router.navigate(['/']);
}
}
