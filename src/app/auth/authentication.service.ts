import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

import { environment } from '../../environments/environment';
import { config } from '../apiConfig';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  private LS = environment.LOCAL_STORAGE;

  allowedRoutes = null;

  private extractData(res: Response): Response | {} {
    const body = res;
    return body || null;
  }

  constructor(private http: HttpClient, private router: Router) {
    if (localStorage.getItem(this.LS) !== null) {
      this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem(this.LS)));
      this.currentUser = this.currentUserSubject.asObservable();
    } else {
      this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem(this.LS)));
      this.currentUser = this.currentUserSubject.asObservable();
    }
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  public login(data: any): Observable<any> {
    return this.http
      .post<any>(config.login, data)
      .pipe(
        map((user) => {
          // login successful if there's a jwt token in the response
          if (user.request_status == 1) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(this.LS, JSON.stringify(user));
            this.currentUserSubject.next(user);
          }
          return user;
        })
      )
      .pipe(catchError(this.handleError));
  }


  public logout(): void {

    // remove user from local storage to log user out
    localStorage.removeItem(this.LS);
    this.currentUserSubject.next(null);
    this.router.navigate(['/login']);

  }

 
  public changePassword(data): Observable<any> {
    return this.http.put(config.change_password, data).pipe(map(this.extractData));
  }

  handleError(error): Observable<never> {
    console.log(error);
    return throwError(error);
  }
}
