import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        let errorurl = '';
        if (error.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Error: ${error.error.message}`;
        } else {
          // server-side error
          if (error.error.data) {
            errorMessage = 'Error: ' + error.error.data.method;
            errorurl = error.error.data.message;
          } else {
            errorMessage = 'Error: Unknown, Check Network Tab in Console';
            errorurl = '';
          }
        }
        // window.alert(errorMessage);

        console.log(errorMessage + ':' + errorurl);

        return throwError(errorMessage);
      })
    );
  }
}
