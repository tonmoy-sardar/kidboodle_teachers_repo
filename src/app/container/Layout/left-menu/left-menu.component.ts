import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/auth/authentication.service';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {
  schoolname:string='';
  constructor(private auth:AuthenticationService) { }

  currentUser = null;
  teacherImage:any

  ngOnInit(): void {
    this.schoolname = localStorage.getItem("school_name");
    this.currentUser = this.auth.currentUserValue;
    this.teacherImage= localStorage.getItem('TeacherImage');

  }

  openNav() {
    let element = document.getElementById('mySidepanel') as HTMLElement;
    element.style.width = "250px";
  }

  closeNav() {
    let element = document.getElementById('mySidepanel') as HTMLElement;
    element.style.width = "0px";
   // document.getElementById("mySidepanel").style.width = "0";
  }

  logout(): void {
    localStorage.clear();
    this.auth.logout();
  }

}
