import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

import { environment } from '../../environments/environment';
import { config } from '../apiConfig';

@Injectable({
  providedIn: 'root',
})
export class StudentService {
  // Setting API call URL from Global File
  private url = environment.DevURL;
  userdetails: any = JSON.parse(localStorage.getItem('KID_DOODLE_USER'))
  allowedRoutes = null;

  private extractData(res: Response): Response | {} {
    const body = res;
    return body || null;
  }

  constructor(private http: HttpClient) {}

  public teacher_student_class_list(): Observable<any> {   
    return this.http
      .get(config.classdetails)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public teacher_class_section_list(classid:any): Observable<any> {  
    console.log(this.userdetails.result.user_details.session.id);
    let paramData = {
      session : this.userdetails.result.user_details.session.id,
      class : classid
    } 
    return this.http
      .get(config.sectiondetails,{params : paramData}  )
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public teacher_student_details_get(searchData:any): Observable<any> {  
    
    searchData.session = this.userdetails.result.user_details.session.id
    return this.http
      .get(config.studentlist,{params : searchData}  )
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  handleError(error): Observable<never> {
    return throwError(error);
  }
}
