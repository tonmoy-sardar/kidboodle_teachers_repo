import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { config } from '../apiConfig'; 

@Injectable({
  providedIn: 'root',
})
export class DashboardService {

  private extractData(res: Response): Response | {} {
    const body = res;
    return body || null;
  }

  constructor(private http: HttpClient) {}

  public teacher_dashboard_details(): Observable<any> {
    return this.http
      .get(config.teacher_dashboard_details)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  handleError(error): Observable<never> {
    return throwError(error);
  }
}
