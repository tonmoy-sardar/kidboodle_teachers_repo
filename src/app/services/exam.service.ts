import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { config } from '../apiConfig'; 


@Injectable({
  providedIn: 'root'
})
export class ExamService {

  private extractData(res: Response): Response | {} {
    const body = res;
    return body || null;
  }

  constructor(private http: HttpClient) {}

  // public teacher_homework_post(data): Observable<any> {
  //   return this.http
  //     .post(config.teacher_homework_post,data)
  //     .pipe(map(this.extractData))
  //     .pipe(catchError(this.handleError));
  // }

  // public teacher_homework_view_list(params: any): Observable<any> {
  //   return this.http
  //     .get(config.teacher_homework_view_list, { params: params })
  //     .pipe(map(this.extractData))
  //     .pipe(catchError(this.handleError));
  // }

  // public teacher_homework_question_post(data): Observable<any> {
  //   return this.http
  //     .post(config.teacher_homework_question_post,data)
  //     .pipe(map(this.extractData))
  //     .pipe(catchError(this.handleError));
  // }

  // public teacher_homework_active_inactive(id,data): Observable<any> {
  //   return this.http
  //     .put(config.teacher_homework_active_inactive + id + '/' ,data)
  //     .pipe(map(this.extractData))
  //     .pipe(catchError(this.handleError));
  // }

  handleError(error): Observable<never> {
    return throwError(error);
  }
}