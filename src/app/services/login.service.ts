import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  httpHeaderOptions: any;
  httpHeaders: HttpHeaders;
  DevURL: string = environment.DevURL;
  headers: any;
  authHeaders;
  constructor(
    private http: HttpClient
  ) {

    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
    })

    this.authHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    })
   }

   getCnfpswdData(body:any ): Observable<any> {
    return this.http.post<any[]>( environment.DevURL + "forgot_password/",body, { headers: this.authHeaders })
  }


  getphone_number_verify(body:any ): Observable<any> {
    return this.http.post<any[]>( environment.DevURL + "phone_number_verify/",body, {headers: this.authHeaders })
  }

  
  getemail_verify(body:any ): Observable<any> {
    return this.http.post<any[]>( environment.DevURL + "email_verify/",body, { headers: this.authHeaders });
  }
  emailExists(body:any ): Observable<any> {
    return this.http.post<any[]>( environment.DevURL + "email_exists_or_not/",body, { headers: this.authHeaders })
  }
  changePass(body:any ): Observable<any> {
    return this.http.put<any[]>( environment.DevURL + "change_password/",body, { headers: this.headers })
  }
}
