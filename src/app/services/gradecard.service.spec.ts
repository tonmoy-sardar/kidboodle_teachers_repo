import { TestBed } from '@angular/core/testing';

import { GradecardService } from './gradecard.service';

describe('GradecardService', () => {
  let service: GradecardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GradecardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
