import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { config } from '../apiConfig'; 
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AttendanceService {
  httpHeaderOptions: { headers: HttpHeaders; };
  headers;
  constructor(private http:HttpClient) { 
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("token")
    })
  }

  getAttendanceDetails(param): Observable<any> {
    // console.log(param);
    return this.http.get<any[]>(environment.DevURL+ "teacher_student_details_get/",{ params: param, headers: this.headers });
  }

}
