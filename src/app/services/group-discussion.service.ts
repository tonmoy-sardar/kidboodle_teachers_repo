import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class GroupDiscussionService {
  headers;
  constructor(
    private http:HttpClient
  ) { 
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      // 'Access-Control-Allow-Origin': '*',
      // 'Access-Control-Allow-Headers': 'Content-Type',
      // 'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
      'Authorization': 'Token ' + localStorage.getItem("token"),
    })
  }

  creategroupDiscussion(body): Observable<any>{
    return this.http.post<any[]>(environment.DevURL+'teacher_group_discussion_create/',body,this.headers)
  }

  getAllGroupDiscussion(params): Observable<any>{
    return this.http.get<any[]>(environment.DevURL+'teacher_group_discussion_create/?'+params,this.headers);
  }

  updateGroupDiscussion(id,body): Observable<any>{
    return this.http.put<any[]>(environment.DevURL+'teacher_group_discussion_edit/'+id +'/',body,this.headers);
  }
  deleteGroupDisscussion(id): Observable<any>{
    return this.http.delete<any[]>(environment.DevURL+'teacher_group_discussion_delete/'+id +'/',this.headers);
  }
   
}
