import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { config } from '../apiConfig';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  httpHeaderOptions: { headers: HttpHeaders; };
  headers;
  headers2;
  private extractData(res: Response): Response | {} {
    const body = res;
    return body || null;
  }

  constructor(private http: HttpClient,private snackBar:MatSnackBar) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("token")
    })
    this.headers2 = new HttpHeaders({
      'Content-Type': 'multipart/form-data',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("token")
    })
  }
  
  allWeekDays(){
    let alldays = [
      {id:0, day:'Sun',day_name:'sunday'},
      {id:1, day:'Mon',day_name:'monday'},
      {id:2, day:'Tue',day_name:'tuesday'},
      {id:3, day:'Wed',day_name:'wednesday'},
      {id:4, day:'Thu',day_name:'thursday'},
      {id:5, day:'Fri',day_name:'friday'},
      {id:6, day:'Sat',day_name:'saturday'},
    ]
    return alldays;
  }
  public getClassDetailsdata(params: any): Observable<any> {
    return this.http
      .get(config.teacher_student_class_list, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  

  public school_class_section_crud(params: any): Observable<any> {
    return this.http
      .get(config.teacher_class_section_list, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public school_subject_class_crud(params: any): Observable<any> {
    return this.http
      .get(config.teacher_class_subject_list, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  public teacher_routine_list_by_daywise(params: any): Observable<any> {
    return this.http
      .get(config.teacher_routine_list_by_daywise, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public formatDate(date): string {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  public attendence_check(param): Observable<any> {
    return this.http.get<any[]>(environment.DevURL+ "teacher_student_attendence_check/",{ params: param, headers: this.headers });
  }
  public attendence_student_list_get(param): Observable<any> {
    return this.http.get<any[]>(environment.DevURL+ "teacher_student_details_get/",{ params: param, headers: this.headers });
  }

  public attendence_teacher_student_list_get(param): Observable<any> {
    return this.http.get<any[]>(environment.DevURL+ "teacher_student_attendence_get/",{ params: param, headers: this.headers });
  }

  public student_attendence(body:any ): Observable<any> {
    return this.http.post<any[]>( environment.DevURL + "teacher_student_attendence_post/",body, { headers: this.headers })
  }
  public teacher_lecture(param): Observable<any> {
    return this.http.get<any[]>( environment.DevURL + "teacher_create_lecture/", { params: param, headers: this.headers })
  }
  public teacher_lecture_post(body:any ): Observable<any> {
    return this.http.post<any[]>( environment.DevURL + "teacher_create_lecture/",body,this.httpHeaderOptions)
  }

  handleError(error): Observable<never> {
    return throwError(error);
  }
  
  openSnackBar(message: string, action: string,status:boolean) {
    this.snackBar.open(message, action,{
      duration: 5000,
      verticalPosition:'top',
      horizontalPosition:'right',
      panelClass: [status === true ? 'snack-bar-class' : 'snack-bar-class-error']
    });
  }
  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        control.controls.forEach(c => this.markFormGroupTouched(c));
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && (form.get(field).dirty || form.get(field).touched);
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'is-invalid': form.get(field).invalid && (form.get(field).dirty || form.get(field).touched),
      'is-valid': form.get(field).valid && (form.get(field).dirty || form.get(field).touched)
    };
  }
  public teacher_exam_type_list(params: any): Observable<any> {
    return this.http
      .get(config.teacher_exam_type_list, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public teacher_student_exam_schedule_list(params: any): Observable<any> {
    return this.http
      .get(config.teacher_student_exam_schedule_list, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public teacher_exam_routine_create(params: any): Observable<any> {
    return this.http
      .get(config.teacher_exam_routine_create, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  public school_class_section_student_list(param): Observable<any> {
    return this.http.get<any[]>( environment.DevURL + "teacher_student_details_get/", { params: param, headers: this.headers })
  }
  
}
