import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { config } from '../apiConfig'; 
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  httpHeaderOptions: { headers: HttpHeaders; };
  httpHeaders: any;
  allowedRoutes = null;

  private extractData(res: Response): Response | {} {
    const body = res;
    return body || null;
  }
  profileEditObj:{address1:string,address2:string,phone:string,country_code:string,state_code:string, city_code:string,pin_code:string}={address1:"",address2:"",phone:"",country_code:"",state_code:"",city_code:"",pin_code:""};
  constructor(private http: HttpClient) {
    this.httpHeaderOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      })
    }
    this.httpHeaders =  new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
    });
    
  }
  stateQueryParams(country_id){
    const params:HttpParams = new HttpParams({
      fromObject:{
        country_id:country_id
      }
    })
    return params;
  }
  cityQueryParams(state_id){
    const params:HttpParams = new HttpParams({
      fromObject:{
        state_id:state_id
      }
    })
    return params;
  }

  public teacher_profile_details(): Observable<any> {
    return this.http
      .get(config.teacher_profile_details, this.httpHeaderOptions)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  getCountryService(){
    return this.http.get(environment.DevURL+'country_code_add/')
  }
  // getting states for a country
  getStateService(country_id:any){
    return this.http.get(environment.DevURL+'state_add_list/?'+this.stateQueryParams(country_id))
  }
  // getting cities for a state
  getCityService(state_id:any){
    return this.http.get(environment.DevURL+'city_add_list/?'+this.cityQueryParams(state_id))
  }
  sendAddress(obj){
    this.profileEditObj= obj;
  }
  putProfileData(params,body):Observable<any>{
    return this.http.put<any[]>(environment.DevURL + "teacher_profile_details/?"+params,body,this.httpHeaderOptions);
  }
  handleError(error): Observable<never> {
    return throwError(error);
  }
  getProfileData(params):Observable<any>{
    return this.http.get<any[]>(environment.DevURL + "teacher_student_academic_details/",{params:params,headers: this.httpHeaders});
  }
}
