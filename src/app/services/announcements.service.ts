import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

import { config } from '../apiConfig'; 

@Injectable({
  providedIn: 'root'
})
export class AnnouncementsService {

  private extractData(res: Response): Response | null {
    const body = res;
    return body || null;
  }

  constructor(private http: HttpClient) {}

  public teacher_all_announcement_list_get(page_number,page_size): Observable<any> {
    return this.http
      .get(config.teacher_all_announcement_list_get + '?page='+page_number+'&page_size='+page_size)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  
  public teacher_all_announcement_list_one_get(id): Observable<any> {
    return this.http
      .get(config.teacher_all_announcement_list_get + '?id='+id+'&page_size=0')
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  } 

  public teacher_announcement_post(data): Observable<any> {
    return this.http
      .post(config.teacher_announcement_post, data)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  handleError(error): Observable<never> {
    return throwError(error);
  }
}
