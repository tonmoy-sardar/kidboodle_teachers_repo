import { TestBed } from '@angular/core/testing';

import { HolidaycalendarService } from './holidaycalendar.service';

describe('HolidaycalendarService', () => {
  let service: HolidaycalendarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HolidaycalendarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
