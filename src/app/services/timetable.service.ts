import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { config } from '../apiConfig'; 
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class TimetableService {
  httpHeaderOptions: { headers: HttpHeaders; };
  headers;
  constructor(
    private _http:HttpClient
  )
  {
    // this.httpHeaderOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/json',
    //     'Accept': 'application/json',
    //     'Authorization': 'Token ' + localStorage.getItem("token")
    //   })
    // }
    this.headers = new HttpHeaders({
      // 'Content-Type': 'application/json',
      // 'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("token")
    })
  }  

  getteacherRoutine(param): Observable<any> {
    return this._http.get<any[]>(environment.DevURL+ "teacher_routine_details_web/",{params:param, headers:this.headers});
  }
  gettimeSlot(param){
    return this._http.get<any[]>(environment.DevURL+ "teacher_routine_time_slot/",{params:param, headers:this.headers});
  }
}
