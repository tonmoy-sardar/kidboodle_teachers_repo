import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LiveclassesService {
  
  headers;
  constructor(
    private http:HttpClient
  ) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("token")
    })
    }

  getRoutineDaywise(params): Observable<any>{
    return this.http.get<any[]>(environment.DevURL+'teacher_routine_list_by_daywise/',{params,headers:this.headers})
  }

  getLiveClass(params): Observable<any>{
    return this.http.get<any[]>(environment.DevURL+'live_class_list/',{params,headers:this.headers})
  }

  createLiveClass(body): Observable<any>{
    return this.http.post<any[]>(environment.DevURL+'live_class_create/',body,this.headers)
  }

}
