import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GradecardService {
  httpHeaderOptions: { headers: HttpHeaders; };
  httpHeaders: any;

  constructor(private _http: HttpClient) {
    this.httpHeaderOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      })
    }
    this.httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
    })
  }

  getTeacherData(params: any): Observable<any> {
    return this._http.get<any[]>(environment.DevURL + "teacher_student_grade_details/", { params: params, headers: this.httpHeaders });
  }
  
  getClassDetailsdata(params: any): Observable<any> {
    return this._http.get<any[]>(environment.DevURL + "teacher_student_class_list/", { params: params, headers: this.httpHeaders });
  }

  getSectiondata(params: any): Observable<any> {
    return this._http.get<any[]>(environment.DevURL + "teacher_class_section_list/", { params: params, headers: this.httpHeaders });
  }

  getStudentDetailsdata(params: any): Observable<any> {
    return this._http.get<any[]>(environment.DevURL + "teacher_student_details_get/", { params: params, headers: this.httpHeaders });
  }

  getAlldata(params: any): Observable<any> {
    return this._http.get<any[]>(environment.DevURL + "teacher_student_grade_details/", { params: params, headers: this.httpHeaders });
  }

  getExamdata(params: any): Observable<any> {
    return this._http.get<any[]>(environment.DevURL + "exam_type_crud/", { params: params, headers: this.httpHeaders });
  }

}
