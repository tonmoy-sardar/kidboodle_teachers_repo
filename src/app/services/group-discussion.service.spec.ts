import { TestBed } from '@angular/core/testing';

import { GroupDiscussionService } from './group-discussion.service';

describe('GroupDiscussionService', () => {
  let service: GroupDiscussionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GroupDiscussionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
