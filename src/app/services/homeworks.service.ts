import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { config } from '../apiConfig'; 


@Injectable({
  providedIn: 'root'
})
export class HomeworksService {
  httpHeaderOptions: { headers: HttpHeaders; };
  headers;
  headers2;
  private extractData(res: Response): Response | {} {
    const body = res;
    return body || null;
  }

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("token")
    })
  }

  public teacher_homework_post(data): Observable<any> {
    return this.http
      .post(config.teacher_homework_post,data)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public teacher_homework_view_list(params: any): Observable<any> {
    return this.http
      .get(config.teacher_homework_view_list, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public homeWorkSubmitWithQuestion(params: any): Observable<any> {
    return this.http
      .get(config.teacher_homework_view_list, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  // For finalized method:
  // http://3.17.248.213:8002/teacher_homework_post/?homework_id=21&method=edit
  // put - method
  // is_finalized: true
  public teacher_homework_postFinal(data: any): Observable<any> {
    return this.http
      .put(config.teacher_homework_post, data)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }


  public questionDeleted(data: any): Observable<any> {
    return this.http
      .put(config.homeWorkDeleted, data)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public teacher_homework_question_post(data): Observable<any> {
    return this.http
      .post(config.teacher_homework_question_post,data)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  
  public teacher_homework_question_details(params): Observable<any> {
    return this.http
      .get(config.teacher_homework_question_details, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }

  public teacher_homework_active_inactive(id,data): Observable<any> {
    return this.http
      .put(config.teacher_homework_active_inactive + id + '/' ,data)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  public teacher_exam_question_post_get(params: any): Observable<any> {
    return this.http
      .get(config.teacher_exam_post, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  public teacher_exam_postFinal(data: any): Observable<any> {
    return this.http
      .put(config.teacher_exam_post, data)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  public teacher_exam_question_post(data): Observable<any> {
    return this.http
      .post(config.teacher_exam_question_post,data)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  public teacher_exam_question_details(params): Observable<any> {
    return this.http
      .get(config.teacher_exam_question_details, { params: params })
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  public teacher_exam_post(data): Observable<any> {
    return this.http
      .post(config.teacher_exam_post,data)
      .pipe(map(this.extractData))
      .pipe(catchError(this.handleError));
  }
  public teacher_submitted_homework_view_list(param): Observable<any> {
    return this.http.get<any[]>( environment.DevURL + "teacher_homework_student_list_update_correct_atch/", { params: param, headers: this.headers })
  }
  public teacher_submitted_homework_attachment_upload(body, params):Observable<any>{
    return this.http.put<any[]>(environment.DevURL + "teacher_homework_student_list_update_correct_atch/?method="+params.method+"&student_id="+params.student_id+"&homework_ans_id="+params.homework_ans_id,body,this.httpHeaderOptions);
  }
  public teacher_submitted_homework_details_view(param): Observable<any> {
    return this.http.get<any[]>( environment.DevURL + "teacher_homework_student_details/", { params: param, headers: this.headers })
  }
  public teacher_online_homework_remarks_submit(body,params): Observable<any> {
    return this.http.put<any[]>( environment.DevURL + "teacher_homework_student_correction/?method="+params.method+"&homework_id="+params.homework_id,body,this.httpHeaderOptions)
  }
  public teacher_submitted_homework_remarks_view(param): Observable<any> {
    return this.http.get<any[]>( environment.DevURL + "teacher_homework_student_details/", { params: param, headers: this.headers })
  }
  public teacher_exam_question_submitted_get(param): Observable<any> {
    return this.http.get<any[]>( environment.DevURL + "teacher_exam_student_list_update_correct_atch/", { params: param, headers: this.headers })
  }
  public teacher_submitted_exam_attachment_upload(body, params):Observable<any>{
    return this.http.put<any[]>(environment.DevURL + "teacher_exam_student_list_update_correct_atch/?method="+params.method+"&student_id="+params.student_id+"&exam_ans_id="+params.exam_ans_id,body,this.httpHeaderOptions);
  }
  public teacher_submitted_exam_details_view(param): Observable<any> {
    return this.http.get<any[]>( environment.DevURL + "teacher_exam_student_details/", { params: param, headers: this.headers })
  }public teacher_online_exam_remarks_submit(body,params): Observable<any> {
    return this.http.put<any[]>( environment.DevURL + "teacher_exam_student_correction/?method="+params.method+"&exam_id="+params.exam_id,body,this.httpHeaderOptions)
  }
  public teacher_submitted_exam_remarks_view(param): Observable<any> {
    return this.http.get<any[]>( environment.DevURL + "teacher_exam_student_details/", { params: param, headers: this.headers })
  }
  handleError(error): Observable<never> {
    return throwError(error);
  }
}