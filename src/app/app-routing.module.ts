import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { AnnouncementsDetailsComponent } from './views/announcements/announcements-details/announcements-details.component';
import { AnnouncementsComponent } from './views/announcements/announcements.component';
import { AnswerSheetsComponent } from './views/answer-sheets/answer-sheets.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { EditprofileComponent } from './views/editprofile/editprofile.component';
import { ExamsComponent } from './views/exam/exams.component';
import { ForgetpasswordComponent } from './views/forgetpassword/forgetpassword.component';
import { GradeCardsComponent } from './views/grade-cards/grade-cards.component';
import { GroupDiscussionsComponent } from './views/group-discussions/group-discussions.component';
import { ListgroupdiscussionsComponent } from './views/group-discussions/listgroupdiscussions/listgroupdiscussions.component';
import { HolidayCalenderComponent } from './views/holiday-calender/holiday-calender.component';
import { AddQuestionsComponent } from './views/homeworks/add-questions/add-questions.component';
import { CreateHomeworkComponent } from './views/homeworks/create-homework/create-homework.component';
import { HomeworksComponent } from './views/homeworks/homeworks.component';
import { SubmittedDetailsComponent } from './views/homeworks/submitted-details/submitted-details.component';
import { LecturesComponent } from './views/lectures/lectures.component';
import { LoginComponent } from './views/login/login.component';
import { MainlayoutComponent } from './views/mainlayout/mainlayout.component';
import { ProfileComponent } from './views/profile/profile.component';
import { StudentAttendanceComponent } from './views/student-attendance/student-attendance.component';
import { StudentComponent } from './views/student/student.component';
import { StudentdetailsComponent } from './views/student/studentdetails/studentdetails.component';
import { TimetableComponent } from './views/timetable/timetable.component';
import { AddExamQuestionsComponent } from './views/exam/add-questions/add-questions.component';
import { ExamAddQuestionComponent } from './views/exam/exam-add-question/exam-add-question.component';
import { CreateExamComponent } from './views/exam/create-exam/create-exam.component';
import { CreateliveclassComponent } from './views/liveclasses/createliveclass/createliveclass.component';
import { LiveclassesComponent } from './views/liveclasses/liveclasses.component';
import { LiveclasshistoryComponent } from './views/liveclasses/liveclasshistory/liveclasshistory.component';
import { ViewdetailsComponent } from './views/grade-cards/viewdetails/viewdetails.component';
import { SubmittedReviewDetailsComponent } from './views/homeworks/submitted-review-details/submitted-review-details.component';
import { SubmittedExamDetailsComponent } from './views/exam/submitted-exam-details/submitted-exam-details.component';
import { SubmittedExamReviewDetailsComponent } from './views/exam/submitted-exam-review-details/submitted-exam-review-details.component';
import { AttendanceDetailsComponent } from './views/student/attendancedetails/attendancedetails.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path:'forgetpassword',
    component:ForgetpasswordComponent
  },
  {
    path: '',
    component: MainlayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashboardComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'edit_profile',
        component: EditprofileComponent
      },
      {
        path: 'announcements',
        component: AnnouncementsComponent,
      },
      {
        path: 'announcements/details/:id',
        component: AnnouncementsDetailsComponent,
      },
      {
        path: 'timetable',
        component: TimetableComponent
      },
      {
        path: 'attendancedetails',
        component: AttendanceDetailsComponent
      },
      {
        path: 'lectures',
        component: LecturesComponent
      },
      {
        path: 'student',
        component: StudentComponent
      },
      {
        path: 'studentdetails',
        component: StudentdetailsComponent
      },
      {
        path: 'student-attendance',
        component: StudentAttendanceComponent
      },
      {
        path: 'homeworks',
        component: HomeworksComponent,
      },
      {
        path: 'homeworks/create-homework',
        component: CreateHomeworkComponent,
      },
      {
        path: 'homeworks/create-homework/add-questions',
        component: AddQuestionsComponent,
      },
      {
        path: 'homeworks/submitted-details',
        component: SubmittedDetailsComponent,
      },
      {
        path: 'group-discussions',
        component: GroupDiscussionsComponent
      },
      { path: 'group-discussions-history',
      component:ListgroupdiscussionsComponent
      },
      {
        path: 'answer-sheets',
        component: AnswerSheetsComponent
      },
      {
        path: 'grade-cards',
        component: GradeCardsComponent
      },
      {
        path: 'holiday-calender',
        component: HolidayCalenderComponent
      },
      {
        path: 'exams',
        component: ExamsComponent
      },
      {
        path: 'grade-cards/viewdetails',
        component: ViewdetailsComponent
      },
      {
        path: 'exam/create-exam',
        component: CreateExamComponent,
      },
      {
        path: 'exam/create-exam/add-questions',
        component: ExamAddQuestionComponent,
      },
      {
        path: 'live-classes', 
        component:LiveclassesComponent
       },
       {path: 'live-classes-history',
       component:LiveclasshistoryComponent
        },
         {​​​​​​ 
          path: 'create-live-classes',
           component:CreateliveclassComponent },
        {
          path: 'homeworks/submitted-remarks-details',
          component:SubmittedReviewDetailsComponent
        },
        {
          path: 'exam/submitted-exam-details',
          component:SubmittedExamDetailsComponent
        },
        {
          path: 'exam/submitted-exam-remarks-details',
          component:SubmittedExamReviewDetailsComponent
        }



    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
