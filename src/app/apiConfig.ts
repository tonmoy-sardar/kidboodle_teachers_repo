import { environment } from "src/environments/environment";

export const config = {

  // Auth URLs
  login: environment.DevURL + 'login/',
  change_password: environment.DevURL + 'change_password/',

  // Dashboard URLs
  teacher_dashboard_details: environment.DevURL + 'teacher_dashboard_details/',

  //Profile URLs
  teacher_profile_details: environment.DevURL + 'teacher_profile_details/',

  // Announcement URLs
  teacher_all_announcement_list_get: environment.DevURL + 'teacher_all_announcement_list_get/',
  teacher_announcement_post: environment.DevURL + 'teacher_announcement_post/',
  teacher_class_section_list: environment.DevURL + 'teacher_class_section_list/',

  // student 
  classdetails: environment.DevURL + 'teacher_student_class_list/',
  sectiondetails: environment.DevURL + 'teacher_class_section_list/',
  studentlist: environment.DevURL + 'teacher_student_details_get/',

  //Homework URLs
  teacher_homework_post: environment.DevURL + 'teacher_homework_post/',
  teacher_homework_question_post: environment.DevURL + 'teacher_homework_question_post/',
  homeWorkDeleted: environment.DevURL + 'teacher_homework_question_post/',
  teacher_homework_view_list: environment.DevURL + 'teacher_homework_view_list/',
  teacher_homework_active_inactive: environment.DevURL + 'teacher_homework_active_inactive/',
  teacher_homework_question_details: environment.DevURL + 'teacher_homework_question_details/',

  // Shared
  school_class_crud: environment.DevURL + 'school_class_crud/?',
  school_class_section_crud: environment.DevURL + 'school_class_section_crud/?',
  school_subject_class_crud: environment.DevURL + 'school_subject_class_crud/?',
  teacher_routine_list_by_daywise: environment.DevURL + 'teacher_routine_list_by_daywise/?',


  //teacher student class list
  teacher_student_class_list: environment.DevURL + 'teacher_student_class_list/?',
  teacher_class_subject_list: environment.DevURL + 'teacher_class_subject_list/?',
  teacher_exam_type_list: environment.DevURL + 'teacher_exam_type_list/?',
  teacher_student_exam_schedule_list: environment.DevURL + 'teacher_student_exam_schedule_list/?',
  teacher_exam_routine_create: environment.DevURL + 'teacher_exam_routine_create/?',
  //exam
  teacher_exam_postFinal: environment.DevURL + 'teacher_exam_post/?',
  teacher_exam_question_details: environment.DevURL + 'teacher_exam_question_details/?',

  teacher_exam_question_post: environment.DevURL + 'teacher_exam_question_post/?',
  teacher_exam_post: environment.DevURL + 'teacher_exam_post/',
}